﻿CREATE PROCEDURE [dbo].[AddToSelfTest]
	@userId NVARCHAR(50),
	@status NVARCHAR(50),
	@appVersion NVARCHAR(128),
	@manufacturer NVARCHAR(128),
	@model NVARCHAR(128),
	@oS INT,
	@operatorId NVARCHAR(128),
	@message NVARCHAR(MAX)
AS
	INSERT INTO [dbo].[SelfTest] VALUES (SYSDATETIMEOFFSET(), @userId, @status, @appVersion, @manufacturer, @model, @oS, @operatorId, @message)
GO
