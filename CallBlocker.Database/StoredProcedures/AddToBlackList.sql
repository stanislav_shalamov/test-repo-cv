﻿CREATE PROCEDURE [dbo].[AddToBlackList]
	@userId NVARCHAR(50),
	@phoneNumber NVARCHAR(128),
	@notes NVARCHAR(MAX),
	@reason NVARCHAR(MAX),
	@name NVARCHAR(MAX)
AS
	INSERT INTO [dbo].[BlackList] VALUES (@userId, @phoneNumber, @notes, @reason, SYSDATETIMEOFFSET(), @name)
GO
