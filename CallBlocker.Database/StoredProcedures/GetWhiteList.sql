﻿CREATE PROCEDURE [dbo].[GetWhiteList]
	@userId nvarchar(50)
AS
	SELECT * FROM [dbo].[WhiteList]
	WHERE UserId = @userId
GO