﻿CREATE PROCEDURE [dbo].[GetUserPhoneNumber]
	@userId NVARCHAR(50)
AS
	SELECT PhoneNumber FROM [dbo].[User] WHERE Id = @userId
GO
