﻿CREATE PROCEDURE [dbo].[AddToReport]
	@userId NVARCHAR(50),
	@phoneNumber NVARCHAR(128),
	@notes NVARCHAR(MAX),
	@reason NVARCHAR(MAX),
	@name NVARCHAR(MAX)
AS
	INSERT INTO [dbo].[Report] VALUES (@userId, @phoneNumber, @notes, @reason, SYSDATETIMEOFFSET(), @name)
GO
