﻿CREATE PROCEDURE [dbo].[CheckNumberInWhiteList]
	@userId NVARCHAR(50),
	@phoneNumber nvarchar(128)
AS
	IF EXISTS (SELECT PhoneNumber FROM [dbo].[WhiteList] WHERE UserId = @userId AND PhoneNumber = @phoneNumber) SELECT 1
GO