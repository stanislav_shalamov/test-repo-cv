﻿CREATE PROCEDURE [dbo].[GetIncomingCall]
	@phoneNumber NVARCHAR(128)
AS
	SELECT * FROM [dbo].[IncomingCall] WHERE PhoneNumber = @phoneNumber
GO
