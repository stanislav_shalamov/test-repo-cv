﻿CREATE PROCEDURE [dbo].[DeleteFromWhiteList]
	@userId NVARCHAR(50),
	@phoneNumber NVARCHAR(128)
AS
	DELETE FROM [dbo].[WhiteList] WHERE UserId = @userId and PhoneNumber = @phoneNumber
GO
