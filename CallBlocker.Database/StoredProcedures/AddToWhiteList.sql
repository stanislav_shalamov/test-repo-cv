﻿CREATE PROCEDURE [dbo].[AddToWhiteList]
	@userId NVARCHAR(50),
	@phoneNumber NVARCHAR(128),
	@name NVARCHAR(MAX)
AS
	INSERT INTO [dbo].[WhiteList] VALUES (@phoneNumber, @userId, @name, SYSDATETIMEOFFSET())
GO
