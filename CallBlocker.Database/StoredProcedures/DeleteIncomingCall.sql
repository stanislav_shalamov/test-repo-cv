﻿CREATE PROCEDURE [dbo].[DeleteIncomingCall]
	@phoneNumber nvarchar(128)
AS
	DELETE FROM [dbo].[IncomingCall]
	WHERE PhoneNumber = @phoneNumber
GO
