﻿CREATE TABLE [dbo].[Subscription]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserId] NVARCHAR(MAX) NOT NULL, 
    [PurchaseToken] NVARCHAR(MAX) NOT NULL, 
    [SubscriptionStatus] INT NOT NULL,
	[OrderId] NVARCHAR(MAX) NOT NULL, 
    [DateTime] DATETIMEOFFSET NULL
)
