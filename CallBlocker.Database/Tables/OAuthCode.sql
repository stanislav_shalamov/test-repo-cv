﻿CREATE TABLE [dbo].[OAuthCode]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Access_Token] NVARCHAR(MAX) NOT NULL, 
    [Expires_In] NVARCHAR(10) NOT NULL, 
    [Refresh_Token] NVARCHAR(MAX) NOT NULL, 
    [DateTime] DATETIMEOFFSET NOT NULL
)
