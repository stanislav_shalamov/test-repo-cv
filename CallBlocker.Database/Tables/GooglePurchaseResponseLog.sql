﻿CREATE TABLE [dbo].[GooglePurchaseResponseLog]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Response] NVARCHAR(MAX) NOT NULL, 
    [DateTime] DATETIMEOFFSET NOT NULL, 
    [Message] NVARCHAR(MAX) NOT NULL
)
