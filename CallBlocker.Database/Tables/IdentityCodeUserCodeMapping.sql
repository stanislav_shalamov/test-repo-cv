﻿CREATE TABLE [dbo].[IdentityCodeUserCodeMapping]
(
	[Id] BIGINT IDENTITY(1,1),
	[IdentityCode] NVARCHAR(MAX) NOT NULL , 
    [UserCode] NVARCHAR(MAX) NOT NULL, 
    [Email] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_IdentityCodeUserCodeMapping] PRIMARY KEY ([Id]) 
)
