﻿CREATE TABLE [dbo].[IncomingCall]
(
	[Id] BIGINT IDENTITY(1,1),
    [UserId] NVARCHAR(50) NOT NULL, 
    [PhoneNumber] NVARCHAR(128) NOT NULL, 
    [SIM] BIT NULL, 
    [IsContact] BIT NOT NULL DEFAULT 0, 
	[OperatorId] NVARCHAR(50) NULL,
    [DateTime] DATETIMEOFFSET NULL, 
    CONSTRAINT [PK_IncomingCall] PRIMARY KEY ([Id]) 
)
