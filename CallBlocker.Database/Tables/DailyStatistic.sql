﻿CREATE TABLE [dbo].[DailyStatistic]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[DateTime] DATETIMEOFFSET NOT NULL,
	[Count] INT NOT NULL,
	[Type] SMALLINT NOT NULL
)
