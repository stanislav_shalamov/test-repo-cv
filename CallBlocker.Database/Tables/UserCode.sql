﻿CREATE TABLE [dbo].[UserCode]
(
	[Id] BIGINT IDENTITY(1,1),
	[PhoneNumber] NVARCHAR(128) NOT NULL, 
    [Code] INT NOT NULL, 
    CONSTRAINT [PK_UserCode] PRIMARY KEY ([Id]) 
)
