﻿CREATE TABLE [dbo].[FailVerification]
(
	[PhoneNumber] NVARCHAR(128) NOT NULL, 
	[Count] INT NOT NULL, 
    CONSTRAINT [PK_FailVerification] PRIMARY KEY ([PhoneNumber])
)
