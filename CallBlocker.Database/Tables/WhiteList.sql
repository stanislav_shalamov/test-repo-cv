﻿CREATE TABLE [dbo].[WhiteList]
(
	[Id] BIGINT IDENTITY(1,1),
    [PhoneNumber] NVARCHAR(128) NOT NULL, 
    [UserId] NVARCHAR(50) NOT NULL, 
    [Name] NVARCHAR(MAX) NULL, 
    [DateTime] DATETIMEOFFSET NULL, 
    CONSTRAINT [PK_WhiteList] PRIMARY KEY ([Id])
)
