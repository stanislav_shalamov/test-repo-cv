﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.BlobStorage
{
    public class AzureBlobStorageService : IBlobStorageService
    {
        private readonly CloudBlobClient _blobClient;
        private readonly CloudBlobContainer _blobContainer;
        private readonly string _containerName;

        public AzureBlobStorageService(string connectionString, string containerName)
        {
            _containerName = containerName;
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            _blobClient = storageAccount.CreateCloudBlobClient();
            _blobContainer = _blobClient.GetContainerReference(containerName);
        }

        public async Task PutBlob(string blobName, byte[] content)
        {
            await CreateContainerIfNotExist();

            var blockBlob = _blobContainer.GetBlockBlobReference(blobName);
            using (var stream = new MemoryStream(content, false))
            {
                await blockBlob.UploadFromStreamAsync(stream);
            }
        }

        public async Task DeleteBlob(string blobName)
        {
            await CreateContainerIfNotExist();

            var blockBlob = _blobContainer.GetBlockBlobReference(blobName);
            await blockBlob.DeleteIfExistsAsync();
        }

        public async Task DeleteExpiredBlobs()
        {
            var blobs = GetBlobs().ToList();

            foreach (var blob in blobs)
            {
                if (blob.Properties.LastModified.Value.AddDays(10) < DateTimeOffset.UtcNow)
                    await DeleteBlob(blob.Name);
            }
        }

        public async Task CreateContainerIfNotExist()
        {
            CloudBlobContainer blobContainer = _blobClient.GetContainerReference(_containerName);
            await blobContainer.CreateIfNotExistsAsync();
        }

        public IEnumerable<CloudBlob> GetBlobs()
        {
            BlobContinuationToken continuationToken = null;

            int? maxResultsPerQuery = null;

            do
            {
                var response = _blobContainer.ListBlobsSegmentedAsync(string.Empty, true, BlobListingDetails.None, maxResultsPerQuery, continuationToken, null, null).Result;
                continuationToken = response.ContinuationToken;
                foreach (var blob in response.Results.OfType<CloudBlob>())
                {
                    yield return blob;
                }
            }
            while (continuationToken != null);
        }
    }
}
