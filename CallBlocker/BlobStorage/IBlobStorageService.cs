﻿using Microsoft.WindowsAzure.Storage.Blob;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CallBlocker.BlobStorage
{
    public interface IBlobStorageService
    {
        /// <summary>
        /// Create blob in blob storage
        /// </summary>
        /// <param name="blobName"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        Task PutBlob(string blobName, byte[] content);
        /// <summary>
        /// Delete blob from blob storage by name
        /// </summary>
        /// <param name="blobName"></param>
        /// <returns></returns>
        Task DeleteBlob(string blobName);

        Task CreateContainerIfNotExist();

        Task DeleteExpiredBlobs();

        IEnumerable<CloudBlob> GetBlobs();
    }
}
