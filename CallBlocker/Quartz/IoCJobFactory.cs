﻿using Quartz;
using Quartz.Spi;
using System;

namespace CallBlocker.Quartz
{
    public class IoCJobFactory : IJobFactory
    {
        private readonly IServiceProvider _factory;

        public IoCJobFactory(IServiceProvider factory)
        {
            _factory = factory;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var service = _factory.GetService(bundle.JobDetail.JobType) as IJob;
            return service;
        }

        public void ReturnJob(IJob job)
        {
            if (job is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
    }
}
