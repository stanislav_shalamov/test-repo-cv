﻿using CallBlocker.Models;
using CallBlocker.Models.Enums;
using CallBlocker.Models.Logs;
using CallBlocker.Models.Performance;
using CallBlocker.Models.Purchase;
using CallBlocker.Models.SelftTest;
using CallBlocker.Repositories.Logs;
using CallBlocker.Repositories.Purchase;
using CallBlocker.Services;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBlocker.Quartz
{
    public class SendTodaysDeveloperStatisticJob : IJob
    {
        private readonly EmailSettings _emailSettings;
        private readonly IDeveloperStatisticService _developerStatisticService;

        public SendTodaysDeveloperStatisticJob(IOptions<EmailSettings> emailSettings, IDeveloperStatisticService developerStatisticService)
        {
            _emailSettings = emailSettings.Value;
            _developerStatisticService = developerStatisticService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await _developerStatisticService.SendLogsDeveloperStatistic(_emailSettings.DevStatisticSubscribers);
        }
    }
}
