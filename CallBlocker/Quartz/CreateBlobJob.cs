﻿using CallBlocker.BlobStorage;
using CallBlocker.Models.Logs;
using CallBlocker.Repositories.Logs;
using CsvHelper;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CallBlocker.Quartz
{
    public class CreateBlobJob : IJob
    {
        private readonly IBlobStorageService _blobStorageService;
        private readonly ILogRepository _logRepository;

        public CreateBlobJob(IBlobStorageService blobStorageService, ILogRepository logRepository)
        {
            _blobStorageService = blobStorageService;
            _logRepository = logRepository;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var currentDate = DateTime.UtcNow;
            var logs = await _logRepository.GetLogs();

            byte[] dataArray;
            using (var memoryStream = new MemoryStream())
            {

                using (ExcelPackage package = new ExcelPackage(memoryStream))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add(string.Concat(currentDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    ws.Cells["A1"].LoadFromCollection(logs, true, TableStyles.Medium2);
                    package.SaveAs(memoryStream);
                }
                dataArray = memoryStream.ToArray();

                // using (var streamWriter = new StreamWriter(memoryStream))
                // using (var csvWriter = new CsvWriter(streamWriter))
                // {
                //     csvWriter.WriteRecords(logs);
                // }
                //
                // dataArray = memoryStream.ToArray();
            }

            await _blobStorageService.PutBlob(string.Concat(currentDate.ToString("yyyy-MM-dd HH:mm:ss"), ".xlsx"), dataArray);
            // await _blobStorageService.PutBlob(string.Concat(currentDate.ToString("yyyy-MM-dd HH:mm:ss"), ".csv"), dataArray);
            await _logRepository.DeleteTodaysLogs(currentDate); 
        }
    }
}
