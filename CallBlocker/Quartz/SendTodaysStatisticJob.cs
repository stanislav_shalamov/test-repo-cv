﻿using CallBlocker.Services;
using Quartz;
using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using CallBlocker.Models.Enums;
using CallBlocker.Repositories.Account;
using Microsoft.Extensions.Options;
using CallBlocker.Models;

namespace CallBlocker.Quartz
{
    public class SendTodaysStatisticJob : IJob
    {
        private readonly IEmailSender _emailSender;
        private readonly IStatisticService _statisticService;
        private readonly IAccountRepository _accountRepository;
        private readonly EmailSettings _emailSettings;

        public SendTodaysStatisticJob(IEmailSender emailSender, IStatisticService statisticService, 
            IAccountRepository accountRepository, IOptions<EmailSettings> emailSettings)
        {
            _emailSender = emailSender;
            _statisticService = statisticService;
            _accountRepository = accountRepository;
            _emailSettings = emailSettings.Value;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var currentDate = DateTime.UtcNow.AddHours(-7);
            var selfTestStatistic = await _statisticService.GetTodaysSelfTestStatistic(currentDate);

            var reportsStatistic = await _statisticService.GetTodaysReportsStatistic(currentDate);
            var forwardedCalls = await _statisticService.GetTodaysStatisticByType(currentDate, StatisticType.ForwardedCalls);
            var newVerified = await _statisticService.GetTodaysStatisticByType(currentDate, StatisticType.NewVerified);
            var wrongCode = await _statisticService.GetTodaysStatisticByType(currentDate, StatisticType.FailedByWrondCode);
            var verifiedCalls = await _statisticService.GetTodaysStatisticByType(currentDate, StatisticType.VerifiedCalls);
            var blockedCalls = await _statisticService.GetTodaysStatisticByType(currentDate, StatisticType.BlockedCalls);

            //  User statistics
            var userCount = await _statisticService.GetTodaysUserStatistic(currentDate);

            var stringBuilder = new StringBuilder();

            stringBuilder.Append($"<p style=\"font-size: 25px; text-align: center\"><strong>YOUR DAILY STATISTICS<br>{currentDate.ToString("dddd, dd MMMM yyyy", CultureInfo.GetCultureInfo("en-us"))}</strong></p>");

            //  Reports statistics by category
            stringBuilder.Append($"<div style=\"border: 1px solid #75a3a3; margin-bottom: 10px\">");
            stringBuilder.Append($"<div style=\"background: #75a3a3; font-size: 25px; color: white\">");
            stringBuilder.Append($"<p style=\"margin: 0px; padding: 5px; \"><strong>Self-Test statistics</strong></p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Self-check passed : {selfTestStatistic.PassedReports}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Self-check failed : {selfTestStatistic.FailedReports}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Self-check conditionally passed : {selfTestStatistic.ConditionalyPassed}</p></div>");
            if (!string.IsNullOrEmpty(selfTestStatistic.UserEmails))
                stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Emails with failed status: {selfTestStatistic.UserEmails}</p></div>");
            stringBuilder.Append($"</div>");

            //  Reports statistics by category
            stringBuilder.Append($"<div style=\"border: 1px solid #75a3a3; margin-bottom: 10px\">");
            stringBuilder.Append($"<div style=\"background: #75a3a3; font-size: 25px; color: white\">");
            stringBuilder.Append($"<p style=\"margin: 0px; padding: 5px; \"><strong>Reports statistics by category</strong></p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Business Solicitation : {reportsStatistic.BusinessSolicitation}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Collection Agency : {reportsStatistic.CollectionAgency}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Scam : {reportsStatistic.Scam}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Political Campaign : {reportsStatistic.PoliticalCampaign}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Robocall : {reportsStatistic.Robocall}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Survey : {reportsStatistic.Survey}</p></div>");
            stringBuilder.Append($"</div>");

            //  Calls statistics
            stringBuilder.Append($"<div style=\"border: 1px solid #75a3a3; margin-bottom: 10px\">");
            stringBuilder.Append($"<div style=\"background: #75a3a3; font-size: 25px; color: white\">");
            stringBuilder.Append($"<p style=\"margin: 0px; padding: 5px; \"><strong>Calls statistics</strong></p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Number of forwarded calls : {forwardedCalls}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Number of new verified numbers : {newVerified}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Number of failed verification tests by wrong code : {wrongCode}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Number of verified calls : {verifiedCalls}</p></div>");
            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Number of blocked calls : {blockedCalls}</p></div>");
            stringBuilder.Append($"</div>");

            //  User statistics
            stringBuilder.Append($"<div style=\"border: 1px solid #75a3a3; margin-bottom: 10px\">");
            stringBuilder.Append($"<div style=\"background: #75a3a3; font-size: 25px; color: white\">");
            stringBuilder.Append($"<p style=\"margin: 0px; padding: 5px; \"><strong>User statistics</strong></p></div>");

            stringBuilder.Append($"<div style=\"padding-left: 20px; \"><p>&#9656; Number of Users : {userCount}</p></div>");
            stringBuilder.Append($"</div>");

            var adminEmails = await _accountRepository.GetAdminEmails();

            foreach (var toEmail in adminEmails)
                await _emailSender.SendEmailAsync(toEmail, _emailSettings.Subject, stringBuilder.ToString());
        }
    }
}
