﻿using CallBlocker.BlobStorage;
using Quartz;
using System.Threading.Tasks;

namespace CallBlocker.Quartz
{
    public class DeleteExpiredBlobsJob : IJob
    {
        private readonly IBlobStorageService _blobStorageService;

        public DeleteExpiredBlobsJob(IBlobStorageService blobStorageService)
        {
            _blobStorageService = blobStorageService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await _blobStorageService.DeleteExpiredBlobs();
        }
    }
}
