﻿using CallBlocker.Models.Email;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        Task SendDevStatisticEmailAsync(string email, string subject, string message, List<AttachmentFile> attachments = null);
    }
}
