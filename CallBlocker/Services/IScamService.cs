﻿using CallBlocker.Models;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public interface IScamService
    {
        Task CheckScamCall(BlackListPostModel model);
    }
}
