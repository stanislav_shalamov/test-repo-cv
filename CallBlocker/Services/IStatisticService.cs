﻿using CallBlocker.Models.BlackList;
using CallBlocker.Models.Enums;
using CallBlocker.Models.SelftTest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public interface IStatisticService
    {
        Task<SelfTestDailyStatisticModel> GetTodaysSelfTestStatistic(DateTime date);

        Task<ReportDailyStatiticModel> GetTodaysReportsStatistic(DateTime date);

        Task<int> GetTodaysStatisticByType(DateTime date, StatisticType type);

        Task<int> GetTodaysUserStatistic(DateTime date);

        Task<IEnumerable<SelfTestModel>> GetTodaysSelfTestStatisticDev(DateTime date);
    }
}
