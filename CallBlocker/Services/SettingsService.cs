﻿using CallBlocker.Repositories.Account;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly IAccountRepository _accountRepository;

        public SettingsService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public async Task ResheduleJobWithNewTime(string hoursString, string minutesString)
        {
            var minutes = int.Parse(minutesString);
            var hours = int.Parse(hoursString);

            if (hoursString.Length != 2 || minutes < 0 || minutes > 59)
                throw new ArgumentException("Minutes pattern has wrong format.");

            if (minutesString.Length != 2 || hours < 0 || hours > 23)
                throw new ArgumentException("Hours pattern has wrong format.");

            DateTime arizonaTime = DateTime.Parse(hoursString + ":" + minutesString);
            DateTime utcTime = arizonaTime.AddHours(7);

            string cronExpression = "00 " + utcTime.Minute.ToString() + " " + utcTime.Hour.ToString() + " ? * *";
            await QuartzServicesUtilities.RescheduleJob(cronExpression);
        }

        public void GetTriggerFireTime(string cronExpression, out string hours, out string minutes)
        {
            if (cronExpression == null)
            {
                hours = "--";
                minutes = "--";
            }
            else
            {
                string[] parts = cronExpression.Split(' ');
                var hoursMinutes = parts.Skip(1).Take(2).ToArray();
                DateTime utcTime = DateTime.Parse(hoursMinutes[1] + ":" + hoursMinutes[0]);
                DateTime arizonaTime = utcTime.AddHours(-7);
                hours = arizonaTime.Hour.ToString();
                minutes = arizonaTime.Minute.ToString();
            }
        }

        public async Task SaveAdminEmails(string adminEmails)
        {
            string[] newAdminEmails = adminEmails.Split(';');
            await _accountRepository.SaveAdminEmails(newAdminEmails);
        }
    }
}
