﻿using CallBlocker.Models.Enums;

namespace CallBlocker.Services
{
    public interface ISubscriptionService
    {
        bool SubscriptionIsActive(SubscriptionStatus currentStatus);
    }
}
