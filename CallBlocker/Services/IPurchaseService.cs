﻿using CallBlocker.Models.Purchase;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public interface IPurchaseService
    {
        Task<GoogleRequestResult> ProcessPurchase(PurchaseSettings purchaseSettings, string refreshToken, PurchaseModel token);
        Task<GoogleRequestResult> ProcessPubSubMessage(PubSubDecodedModel pubSubDecodedModel, PurchaseSettings purchaseSettings, string refreshToken);
    }
}
