﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public interface IDeveloperStatisticService
    {
        Task SendLogsDeveloperStatistic(string[] emails);
        Task SendGooglePurchaseLogsDeveloperStatistic(string[] subscribers);
    }
}
