﻿using CallBlocker.Models;
using CallBlocker.Models.Enums;
using CallBlocker.Repositories.BlackList;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public class ScamService : IScamService
    {
        private readonly IBlackListRepository _blackListRepository;

        public ScamService(IBlackListRepository blackListRepository)
        {
            _blackListRepository = blackListRepository;
        }

        public async Task CheckScamCall(BlackListPostModel model)
        {
            var numberInReports = await _blackListRepository.NumberInReports(model.PhoneNumber);
            //var countNumbers = numberInReports?.Count();
            var groupsCount = numberInReports.GroupBy(x => x.UserId).Count();

            if (groupsCount >= SolutionConstants.NumberInLocalBlackLists)
            {
                var scamNumbers = numberInReports.Count(x => x.Reason == ReportReason.Scam.ToString());
                var percentOfScum = (scamNumbers / (double)groupsCount) * 100;

                if (percentOfScum >= SolutionConstants.ScamCallPercent)
                {
                    await _blackListRepository.AddToBlackList(new BlackListPostModel
                    {
                        PhoneNumber = model.PhoneNumber,
                        Reason = model.Reason,
                        UserId = model.UserId,
                        Name = model.Name,
                        Notes = model.Notes
                    });
                }
            }
        }
    }
}
