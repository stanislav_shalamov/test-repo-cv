﻿using CallBlocker.Models.Purchase;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public class GooglePurchaseService : IPurchaseService
    {
        public async Task<GoogleRequestResult> ProcessPurchase(PurchaseSettings purchaseSettings, string refreshToken, PurchaseModel model)
        {
            string requestData = $"grant_type=refresh_token&client_id={purchaseSettings.ClientId}&client_secret={purchaseSettings.ClientSecret}&refresh_token={refreshToken}";
            ResponseModel responseModel = await PerformRequest(SolutionConstants.GetAccessTokenURL, requestData);

            if (!responseModel.IsSuccessful)
                return new GoogleRequestResult()
                {
                    IsSuccessful = false,
                    ErrorMessage = $"Exception GetAccessToken step [UserId] = {model.UserId}",
                    JsonException = responseModel.JsonError
                };

            GoogleAccessDataModel googleAccessModel = JsonConvert.DeserializeObject<GoogleAccessDataModel>(responseModel.JsonResponse);

            string googlePlayRequestUrl = $"https:" +
                $"//www.googleapis.com" +
                $"/androidpublisher/v3/applications/{purchaseSettings.PackageName}/purchases" +
                $"/subscriptions/{model.ProductId}/tokens/{model.Token}?access_token={googleAccessModel.Access_token}";

            responseModel = await PerformRequest(googlePlayRequestUrl);

            if (!responseModel.IsSuccessful)
                return new GoogleRequestResult()
                {
                    IsSuccessful = false,
                    ErrorMessage = $"Exception GetPurchaseDetails step [UserId] = {model.UserId}",
                    JsonException = responseModel.JsonError
                };

            GooglePurchaseModel purchaseModel = JsonConvert.DeserializeObject<GooglePurchaseModel>(responseModel.JsonResponse);

            if (string.IsNullOrEmpty(purchaseModel.OrderId))
                return new GoogleRequestResult()
                {
                    IsSuccessful = false,
                    ErrorMessage = $"PurchaseModel.OrderId is null or empty [UserId] = {model.UserId}",
                    JsonException = responseModel.JsonResponse
                };

            return new GoogleRequestResult()
            {
                IsSuccessful = true,
                OrderId = ParseOrderId(purchaseModel.OrderId),
                JsonResponse = responseModel.JsonResponse
            };
        }

        public async Task<GoogleRequestResult> ProcessPubSubMessage(PubSubDecodedModel pubSubDecodedModel, PurchaseSettings purchaseSettings, string refreshToken)
        {
            var requestData = $"grant_type=refresh_token&client_id={purchaseSettings.ClientId}&client_secret={purchaseSettings.ClientSecret}&refresh_token={refreshToken}";
            ResponseModel responseModel = await PerformRequest("https://accounts.google.com/o/oauth2/token", requestData);

            if (!responseModel.IsSuccessful)
                return new GoogleRequestResult()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Exception PostPubSubMessage GetAccessToken step",
                    JsonException = responseModel.JsonError
                };

            GoogleAccessDataModel googleAccessModel = JsonConvert.DeserializeObject<GoogleAccessDataModel>(responseModel.JsonResponse);

            string googlePlayRequestUrl = $"https:" +
                $"//www.googleapis.com/androidpublisher/v3/applications/" +
                $"{purchaseSettings.PackageName}/purchases/subscriptions/{pubSubDecodedModel.SubscriptionNotification.SubscriptionId}" +
                $"/tokens/{pubSubDecodedModel.SubscriptionNotification.PurchaseToken}?access_token={googleAccessModel.Access_token}";

            responseModel = await PerformRequest(googlePlayRequestUrl);

            if (!responseModel.IsSuccessful)
                return new GoogleRequestResult()
                {
                    IsSuccessful = false,
                    ErrorMessage = $"Exception PostPubSubMessage GetPurchaseDetails step [PurchaseToken] = {pubSubDecodedModel.SubscriptionNotification.PurchaseToken}",
                    JsonException = responseModel.JsonError
                };

            GooglePurchaseModel purchaseModel = JsonConvert.DeserializeObject<GooglePurchaseModel>(responseModel.JsonResponse);
            string orderId = ParseOrderId(purchaseModel.OrderId);

            return new GoogleRequestResult()
            {
                IsSuccessful = true,
                OrderId = orderId
            }; 
        }

        private string ParseOrderId(string orderId)
        {
            string[] parts = orderId.Split('.');

            if (parts.Length >= 2)
                return parts[0] + "." + parts[1];
            return orderId;
        }


        private async Task<ResponseModel> PerformRequest(string url, string requestData = "")
        {
            if (!string.IsNullOrEmpty(requestData))
            {
                WebRequestBuilder POSTRequest = new WebRequestBuilder(url, requestData);
                return await POSTRequest.GetResponse();
            }
            WebRequestBuilder GETRequest = new WebRequestBuilder(url);
            return await GETRequest.GetResponse();
        }
    }
}
