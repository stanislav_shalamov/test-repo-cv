﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CallBlocker.Models.Enums;

namespace CallBlocker.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        public bool SubscriptionIsActive(SubscriptionStatus currentStatus)
        {
            return currentStatus == SubscriptionStatus.Subscription_recovered ||
                    currentStatus == SubscriptionStatus.Subscription_renewed ||
                    currentStatus == SubscriptionStatus.Subscription_canceled ||
                    currentStatus == SubscriptionStatus.Subscription_purchased ||
                     currentStatus == SubscriptionStatus.Subscription_in_grace_period ||
                     currentStatus == SubscriptionStatus.Subscription_restarted ||
                     currentStatus == SubscriptionStatus.Subscription_price_change_confirmed;
        }
    }
}
