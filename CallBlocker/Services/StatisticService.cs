﻿using CallBlocker.Models.BlackList;
using CallBlocker.Models.Enums;
using CallBlocker.Models.SelftTest;
using CallBlocker.Repositories.SelfTest;
using System;
using System.Linq;
using System.Threading.Tasks;
using CallBlocker.Repositories.BlackList;
using CallBlocker.Repositories.Statistic;
using CallBlocker.Repositories.User;
using System.Collections.Generic;

namespace CallBlocker.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly ISelfTestRepository _selfTestRepository;
        private readonly IBlackListRepository _blackListRepository;
        private readonly IStatisticRepository _statisticRepository;
        private readonly IUserRepository _userRepository;


        public StatisticService(ISelfTestRepository selfTestRepository, IBlackListRepository blackListRepository, IStatisticRepository statisticRepository,
            IUserRepository userRepository)
        {
            _selfTestRepository = selfTestRepository;
            _blackListRepository = blackListRepository;
            _statisticRepository = statisticRepository;
            _userRepository = userRepository;
        }

        public async Task<ReportDailyStatiticModel> GetTodaysReportsStatistic(DateTime date)
     {
            var reportsStatistic = await _blackListRepository.GetTodaysReportsStatistic(date);

            var businessSolicitation = reportsStatistic.Count(x => x.Reason == ReportReason.Business_solicitation.ToString().Replace("_", " ")); 
            var politicalCampaign = reportsStatistic.Count(x => x.Reason == ReportReason.Political_campaign.ToString().Replace("_", " "));
            var scam = reportsStatistic.Count(x => x.Reason == ReportReason.Scam.ToString());
            var collectionAgency = reportsStatistic.Count(x => x.Reason == ReportReason.Collection_agency.ToString().Replace("_", " "));
            var robocall = reportsStatistic.Count(x => x.Reason == ReportReason.Robocall.ToString());
            var survey = reportsStatistic.Count(x => x.Reason == ReportReason.Survey.ToString());

            return new ReportDailyStatiticModel(businessSolicitation, politicalCampaign, scam, robocall, survey, collectionAgency);
        }

        public async Task<SelfTestDailyStatisticModel> GetTodaysSelfTestStatistic(DateTime date)
        {
            var selfTestStatistic = await _selfTestRepository.GetTodaysStatistic(date);

            var passedStatistic = selfTestStatistic.Count(x => x.Status == SelfTestStatus.Passed.ToString().ToLower());
            var failedStatistic = selfTestStatistic.Count(x => x.Status == SelfTestStatus.Failed.ToString().ToLower());
            var conditionallyPassedStatistic = selfTestStatistic.Count(x => x.Status == SelfTestStatus.Conditionally_passed.ToString().ToLower());

            var failedEmails = await _selfTestRepository.GetUserEmailsByStatus(date, SelfTestStatus.Failed);

            return new SelfTestDailyStatisticModel(failedStatistic, passedStatistic, conditionallyPassedStatistic, string.Join(", ", failedEmails));
        }

        public async Task<IEnumerable<SelfTestModel>> GetTodaysSelfTestStatisticDev(DateTime date)
        {
           return await _selfTestRepository.GetTodaysSelfTestStatisticDev(date);
        }

        public async Task<int> GetTodaysStatisticByType(DateTime date, StatisticType type)
        {
            var dailyStatistic = await _statisticRepository.GetDailyStatisticByType(date, type);

            return dailyStatistic?.Count ?? 0;
        }

        public async Task<int> GetTodaysUserStatistic(DateTime date)
        {
            return await _userRepository.GetUsersCountByDate(date);
        }
    }
}
