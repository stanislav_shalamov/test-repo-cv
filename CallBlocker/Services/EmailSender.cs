﻿using CallBlocker.Models;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CallBlocker.Models.Email;

namespace CallBlocker.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public EmailSender(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var client = new SendGridClient(_emailSettings.ApiKey);

            var msg = new SendGridMessage
            {
                From = new EmailAddress(_emailSettings.Email, _emailSettings.Name),
                Subject = subject,
                HtmlContent = message
            };

            msg.AddTo(new EmailAddress(email, _emailSettings.Name));

            var response = await client.SendEmailAsync(msg);
        }

        public async Task SendDevStatisticEmailAsync(string email, string subject, string message, List<AttachmentFile> attachments = null)
        {
            var client = new SendGridClient(_emailSettings.ApiKey);

            var msg = new SendGridMessage
            {
                From = new EmailAddress(_emailSettings.Email, "CallVerify - Self-check failures"),
                Subject = subject,
                HtmlContent = message
            };

            msg.AddTo(new EmailAddress(email, "Daily Report"));

            if (attachments != null)
                foreach (var attachment in attachments)
                    msg.AddAttachment(attachment.FileName, attachment.Base64Content);
            var response = await client.SendEmailAsync(msg);
        }
    }
}