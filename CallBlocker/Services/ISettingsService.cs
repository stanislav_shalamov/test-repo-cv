﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public interface ISettingsService
    {
        Task ResheduleJobWithNewTime(string hoursString, string minutesString);

        void GetTriggerFireTime(string cronExpression, out string hours, out string minutes);

        Task SaveAdminEmails(string adminEmails);
    }
}
