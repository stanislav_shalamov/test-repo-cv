﻿using CallBlocker.Models;
using CallBlocker.Models.Email;
using CallBlocker.Models.Enums;
using CallBlocker.Models.Logs;
using CallBlocker.Models.Performance;
using CallBlocker.Models.Purchase;
using CallBlocker.Models.SelftTest;
using CallBlocker.Repositories.Logs;
using CallBlocker.Repositories.Purchase;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBlocker.Services
{
    public class DeveloperStatisticService : IDeveloperStatisticService
    {
        private readonly IEmailSender _emailSender;
        private readonly IStatisticService _statisticService;
        private readonly ILogRepository _logRepository;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly ISubscriptionService _subscriptionService;

        public DeveloperStatisticService(IEmailSender emailSender, IStatisticService statisticService,
            ILogRepository logRepository, IPurchaseRepository purchaseRepository,
    ISubscriptionService subscriptionService)
        {
            _emailSender = emailSender;
            _statisticService = statisticService;
            _logRepository = logRepository;
            _purchaseRepository = purchaseRepository;
            _subscriptionService = subscriptionService;
        }

        public async Task SendLogsDeveloperStatistic(string[] subscribers)
        {
            List<PerformanceMonitor> monitors = new List<PerformanceMonitor>();
            Stopwatch sw = new Stopwatch();

            var fromDateTime = DateTime.UtcNow.AddHours(-24);
            sw.Start();
            IEnumerable<SelfTestModel> selfTestStatistic = await _statisticService.GetTodaysSelfTestStatisticDev(fromDateTime);
            sw.Stop();
            monitors.Add(new PerformanceMonitor { Operation = "GetSelfTestStatistic", Elapsed = sw.Elapsed });
            sw.Reset();

            sw.Start();
            IEnumerable<LogModel> logs = await _logRepository.GetLogs();
            sw.Stop();
            monitors.Add(new PerformanceMonitor { Operation = "GetLogs", Elapsed = sw.Elapsed });
            sw.Reset();

            sw.Start();
            IEnumerable<SubscriptionModel> subscriptions = await _purchaseRepository.Get();
            sw.Stop();
            monitors.Add(new PerformanceMonitor { Operation = "GetSubscriptions", Elapsed = sw.Elapsed });
            sw.Reset();

            var stringBuilder = new StringBuilder();
            stringBuilder.Append($"<p style=\"font-size: 20px; text-align: center\"><strong>Daily Report for developers</strong></p>");

            stringBuilder.Append($"<p style=\"font-size: 20px;\">Reports for the next period of time: </p>");

            var currentDateTime = DateTime.UtcNow;
            string timePattern = "dd.MM HH:mm";
            stringBuilder.Append($"<p style=\"font-size: 20px;\"><strong>Arizona time: </strong> " +
                $"From {currentDateTime.AddHours(-31).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))} " +
                $"to {currentDateTime.AddHours(-7).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))}</p>");

            stringBuilder.Append($"<p style=\"font-size: 20px;\"><strong>UTC time: </strong>" +
            $"From {currentDateTime.AddHours(-24).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))} " +
            $"to {currentDateTime.ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))}</p>");

            stringBuilder.Append($"<p style=\"font-size: 20px;\"><strong>Time in Ukraine: </strong> " +
            $"From {currentDateTime.AddHours(-21).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))} " +
            $"to {currentDateTime.AddHours(3).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))}</p>");
            stringBuilder.Append($"<hr>");

            var activeSubscriptions = subscriptions
                .Where(s => _subscriptionService.SubscriptionIsActive((SubscriptionStatus)s.SubscriptionStatus))
                .GroupBy(s => s.UserId).ToList();

            stringBuilder.Append($"<p style=\"font-size: 18px; color: #008000;\"><strong>Active subscriptions: </strong>" +
                $"{activeSubscriptions.Count()}");

            stringBuilder.Append($"<hr>");

            stringBuilder.Append($"<p><strong>Performance:</strong><br>[operation]:[ElapsedTime]</p>");

            sw.Start();
            List<AttachmentFile> attachments = GetAttachments(selfTestStatistic, logs, subscriptions);
            sw.Stop();
            monitors.Add(new PerformanceMonitor { Operation = "Generate xlsx file", Elapsed = sw.Elapsed });

            foreach (var monitor in monitors)
                stringBuilder.Append($"<p><strong>&#9656; {monitor.Operation}: </strong>{monitor.Elapsed}</p>");

            if (subscribers != null || subscribers.Length != 0)
                foreach (var subscriberEmail in subscribers)
                    await _emailSender.SendDevStatisticEmailAsync(subscriberEmail, "CallVerify - DevStatistic", stringBuilder.ToString(), attachments);
        }

        public async Task SendGooglePurchaseLogsDeveloperStatistic(string[] subscribers)
        {
            var fromDateTime = DateTime.UtcNow.AddHours(-24);
            var logs = await _purchaseRepository.GetLogs();

            var stringBuilder = new StringBuilder();
            stringBuilder.Append($"<p style=\"font-size: 20px; text-align: center\"><strong>Daily Report for developers</strong></p>");

            stringBuilder.Append($"<p style=\"font-size: 20px;\">Reports for the next period of time: </p>");

            var currentDateTime = DateTime.UtcNow;
            string timePattern = "dd.MM HH:mm";
            stringBuilder.Append($"<p style=\"font-size: 20px;\"><strong>Arizona time: </strong> " +
                $"From {currentDateTime.AddHours(-31).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))} " +
                $"to {currentDateTime.AddHours(-7).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))}</p>");

            stringBuilder.Append($"<p style=\"font-size: 20px;\"><strong>UTC time: </strong>" +
            $"From {currentDateTime.AddHours(-24).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))} " +
            $"to {currentDateTime.ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))}</p>");

            stringBuilder.Append($"<p style=\"font-size: 20px;\"><strong>Time in Ukraine: </strong> " +
            $"From {currentDateTime.AddHours(-21).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))} " +
            $"to {currentDateTime.AddHours(3).ToString(timePattern, CultureInfo.GetCultureInfo("en-us"))}</p>");
            stringBuilder.Append($"<hr>");

            stringBuilder.Append($"<p><strong>Performance:</strong><br>[operation]:[ElapsedTime]</p>");


            List<AttachmentFile> attachments = GetAttachments(logs);

            if (subscribers != null || subscribers.Length != 0)
                foreach (var subscriberEmail in subscribers)
                    await _emailSender.SendDevStatisticEmailAsync(subscriberEmail, "CallVerify - DevStatistic Googe purchase log", stringBuilder.ToString(), attachments);
        }

        private List<AttachmentFile> GetAttachments(IEnumerable<PurchaseLogModel> logs)
        {
            var attachments = new List<AttachmentFile>();
            string fileName = "PurchaseLogs " + DateTime.UtcNow.ToString("dd.MM.yy", CultureInfo.GetCultureInfo("en-us"));
            AttachmentFile file = GenerateFile(logs, fileName);
            attachments.Add(file);
            return attachments;
        }

        private AttachmentFile GenerateFile(IEnumerable<PurchaseLogModel> logs, string fileName)
        {
            byte[] dataArray;
            using (var memoryStream = new MemoryStream())
            {
                using (ExcelPackage package = new ExcelPackage(memoryStream))
                {
                        ExcelWorksheet ws = package.Workbook.Worksheets.Add("[GooglePurchaseResponseLog]");
                        AddTableToExcelWorksheet(ws, "A", "C", "Logs ", $"COUNT = {logs.Count()}", TableStyles.Light1, logs);
                    
                    package.SaveAs(memoryStream);
                }
                dataArray = memoryStream.ToArray();
            }
            return new AttachmentFile() { Base64Content = Convert.ToBase64String(dataArray), FileName = fileName + "(UTC).xlsx" };
        }

        private List<AttachmentFile> GetAttachments(IEnumerable<SelfTestModel> selfTestStatistic, IEnumerable<LogModel> logs, IEnumerable<SubscriptionModel> subscriptions)
        {
            var attachments = new List<AttachmentFile>();
            string fileName = DateTime.UtcNow.ToString("dd.MM.yy_hh-mm", CultureInfo.GetCultureInfo("en-us"));
            AttachmentFile file = GenerateFile(selfTestStatistic, logs, subscriptions, fileName);
            attachments.Add(file);
            return attachments;
        }

        private AttachmentFile GenerateFile(IEnumerable<SelfTestModel> selfTestStatistic, IEnumerable<LogModel> logs, 
            IEnumerable<SubscriptionModel> subscriptions, string fileName)
        {
            var selfTestStatisticGroups = selfTestStatistic.GroupBy(model => model.UserId);
            var logGroups = logs.GroupBy(model => model.UserId);

            byte[] dataArray;
            using (var memoryStream = new MemoryStream())
            {
                using (ExcelPackage package = new ExcelPackage(memoryStream))
                {
                    foreach (var key in selfTestStatisticGroups)
                    {
                        int index = key.Count() + 4;    //Additional Rows count = 4
                        string email = key.First().Email;
                        ExcelWorksheet ws = package.Workbook.Worksheets.Add(email);
                        AddTableToExcelWorksheet(ws, "A", "J", "SelfTest", $"{email}", TableStyles.Light1, key);

                        ws.Cells["A" + index].LoadFromText("Log");
                        SetCellStyle(ws, "A" + index + ":J" + index);
                        index++;
                        ws.Cells["A" + index].LoadFromCollection(logs.Where(log => log.UserId == key.First().UserId), true, TableStyles.Light1);
                    }

                    ExcelWorksheet logsWs = package.Workbook.Worksheets.Add("Logs");
                    AddTableToExcelWorksheet(logsWs, "A", "C", "LOG", $"Count = {logs.Count()}", TableStyles.Medium2, logs);

                    if (subscriptions != null)
                    {   
                        ExcelWorksheet ws = package.Workbook.Worksheets.Add("Subscriptions");
                        AddTableToExcelWorksheet(ws, "A", "F", "Subscriptions", $"COUNT = {subscriptions.Count()}", TableStyles.Light1, subscriptions);

                        var uniqueRows = subscriptions.GroupBy(item => item.UserId)
                            .Select(m => new { UserId = m.Key, m.First().Email, Status = string.Join(", ", m.Select(i => i.SubscriptionStatus)) })
                            .OrderBy(m => m.Email);

                        AddTableToExcelWorksheet(ws, "H", "J", "Subscriptions - Unique rows", $"COUNT = {uniqueRows.Count()}", TableStyles.Light21, uniqueRows);

                        var activeSubscriptions = subscriptions
                            .Where(s => _subscriptionService.SubscriptionIsActive((SubscriptionStatus)s.SubscriptionStatus))
                            .GroupBy(s => s.UserId).ToList()
                            .Select(s => new { UserId = s.Key, Email = s.First().Email, Status = string.Join(", ", s.Select(i => i.SubscriptionStatus)) })
                            .OrderBy(s => s.Email);

                        AddTableToExcelWorksheet(ws, "L", "N", "Active Subscriptions", $"COUNT = {activeSubscriptions.Count()}", TableStyles.Light21, activeSubscriptions);

                        var statusList = Enum.GetValues(typeof(SubscriptionStatus))
                           .Cast<SubscriptionStatus>()
                           .Select(e => new { SubscriptionStatus = e.ToString(), Value = (int)e, IsActive = _subscriptionService.SubscriptionIsActive(e) })
                           .ToList();

                        AddTableToExcelWorksheet(ws, "P", "R", "Subscriptions code", "", TableStyles.Light20, statusList);
                    }
                    package.SaveAs(memoryStream);
                }
                dataArray = memoryStream.ToArray();
            }
            return new AttachmentFile() { Base64Content = Convert.ToBase64String(dataArray), FileName = fileName + "(UTC).xlsx" };
        }

        private void AddTableToExcelWorksheet<T>(ExcelWorksheet ws, string topLeftCell, string topRightCell, string title1, string title2,
            TableStyles style, IEnumerable<T> list)
        {
            ws.Cells[topLeftCell + "1"].LoadFromText(title1);
            ws.Cells[topLeftCell + "2"].LoadFromText(title2);
            SetCellStyle(ws, topLeftCell + "1:" + topRightCell + "1");
            SetCellStyle(ws, topLeftCell + "2:" + topRightCell + "2");
            ws.Cells[topLeftCell + "3"].LoadFromCollection(list, true, style).AutoFitColumns();
        }

        private void SetCellStyle(ExcelWorksheet ws, string position, bool fillColor = true)
        {
            ws.Cells[position].Merge = true;
            ws.Cells[position].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Cells[position].Style.Font.Bold = true;
            if (fillColor)
            {
                ws.Cells[position].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[position].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 208, 142));
            }
        }
    }
}
