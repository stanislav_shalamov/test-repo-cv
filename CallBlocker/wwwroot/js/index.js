﻿{
    var emailArray = new Array();

    function Button(item) {

        function click() {
            var divId = item.parentNode.id;
            emailArray.splice(divId, 1);
            if (emailArray.length == 0) {
                HideInputById(document.getElementById("inputSaveInForm"));
            } else {
                ShowInputById(document.getElementById("inputSaveInForm"));
            }
            ShowEmailList();
        }
        item.addEventListener('click', click);
    }

    function ShowEmailList() {

        var container = document.getElementById("container");
        while (container.firstChild) {
            container.firstChild.remove();
        }

        for (var i = 0; i < emailArray.length; i++) {

            var div1 = document.createElement("div");
            div1.className = "index-container-email-item";
            div1.id = i;

            var spanText = document.createElement("span");
            spanText.textContent = emailArray[i];
            spanText.style.padding = "5px";

            var spanImage = document.createElement("span");
            spanImage.className = "glyphicon glyphicon-remove-circle";
            spanImage.style.color = "darkred";

            div1.appendChild(spanText);
            div1.appendChild(spanImage);

            container.appendChild(div1);

            new Button(spanImage);
        }
    }

    function AddEmail(newEmail) {
        emailArray.push(newEmail);
        ShowEmailList();
    }

    function ClearEmailList() {
        var container = document.getElementById("container");
        while (container.firstChild) {
            container.firstChild.remove();
        }
    }

    function SaveTodayStatisticSettings() {
        var notifyEmails = "";
        var notifyTime = "";

        var inputHours = document.getElementById("sendTodayStatisticHoursInput");
        var inputMinutes = document.getElementById("sendTodayStatisticMinutesInput");

        notifyTime = inputHours.value + ":" + inputMinutes.value;

        for (var i = 0; i < emailArray.length; i++) {
            notifyEmails = notifyEmails + emailArray[i];
            if (i != emailArray.length - 1) {
                notifyEmails = notifyEmails + ";";
            }
        }

        var statisticModel = JSON.stringify({
            SendTodayStatisticHours: inputHours.value, SendTodayStatisticMinutes: inputMinutes.value,
            AdminEmailsString: notifyEmails
        });

        HideInputById(document.getElementById("inputSaveInForm"));

        var container = document.getElementById("result");
        var span = document.createElement("span");
        span.style.color = "#BCBCBC";
        span.style.fontSize = "20px";
        span.textContent = "Processing";

        container.appendChild(span);

        var cookie = document.cookie.match('CallBlockerValidationKey');
        if (cookie == null) {
            window.location.href = '/api/Settings/Login';
        }

        $.ajax({
            url: "/api/Settings/SaveSettings",
            type: 'POST',
            dataType: 'text',
            contentType: 'application/json; charset=utf-8',
            data: statisticModel,
            success: function (data) {
                $('#result').html(data);
                RemoveElementByIDFromContainer(document.getElementById("result"))

            },
            error: function () {
                $('#result').html("<span style =\"color: darkred; font-size: 20px\">Something went wrong. Can`t save data!</span>");

            }
        });
    }

    function RemoveElementByIDFromContainer(container) {
        setTimeout(function () {
            while (container.firstChild) {
                container.firstChild.remove();
            }
        }, 6000);
    }

    function HideInputById(inputId) {
        if (inputId != null) {
            inputId.type = "hidden";
        }
    }

    function ShowInputById(inputId) {
        if (inputId != null) {
            inputId.type = "submit";
        }
    }



    window.onload = function () {
        var inputEmailTxt = document.getElementById("inputEmailTxtArea");
        var inputHours = document.getElementById("sendTodayStatisticHoursInput");
        var inputMinutes = document.getElementById("sendTodayStatisticMinutesInput");

        if (inputEmailTxt != null) {
            inputEmailTxt.onkeydown = function (e) {
                if (e.keyCode == 13) {

                    e.preventDefault();
                    var val = inputEmailTxt.value;
                    if (val.includes("@")) {
                        AddEmail(val);
                        inputEmailTxt.value = "";
                        ShowInputById(document.getElementById("inputSaveInForm"));
                    }
                    return false;
                }
            }
            ShowEmailList();
        }

        if (inputHours != null) {
            inputHours.onkeyup = function (e) {
                var hours = parseInt(inputHours.value, 10);
                if (hours > 23 || inputHours.value < 0 || inputHours.value.length > 2) {
                    inputHours.value = "23";
                }
            }

            inputHours.onfocus
        }


        if (inputMinutes != null) {
            inputMinutes.onkeyup = function (e) {
                var minutes = parseInt(inputMinutes.value, 10);
                if (minutes > 59 || inputHours.value < 0) {
                    inputMinutes.value = "59";
                }
                if (inputMinutes.value.length > 2) {
                    inputMinutes.value = "59";
                }
            }
        }
        LoadData();
    }

    function LoadData() {
        $.ajax({
            url: "/api/Settings/Get",
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (datajson) {
                emailArray = datajson.adminEmails.split(";");
                ShowEmailList();
            },
            error: function () {
                $('#result').html("<span style =\"color: darkred; font-size: 20px\">Something went wrong. Server error.</span>");
            }
        });
    }

    function CheckHoursMinutesInput() {
        var inputHours = document.getElementById("sendTodayStatisticHoursInput");
        var inputMinutes = document.getElementById("sendTodayStatisticMinutesInput");

        if (inputHours.value.length == 0) {
            inputHours.value = "23";
        }
        if (inputHours.value.length == 1) {
            inputHours.value = "0" + inputHours.value;
        }

        if (inputMinutes.value.length == 0) {
            inputMinutes.value = "59";
        }
        if (inputMinutes.value.length == 1) {
            inputMinutes.value = "0" + inputMinutes.value;
        }
    }


}