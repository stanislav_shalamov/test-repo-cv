﻿var signInBtn;

window.onload = function () {
    signInBtn = document.getElementById("inputSignin");
    signInBtn.addEventListener("click", HideSignInButton);
};

function HideSignInButton() {

    if (signInBtn == null) {
        return;
    }

    signInBtn.type = "hidden";
    var container = document.getElementById("signInInfoDiv");;

    var p = document.createElement("p");
    p.style.color = "#BCBCBC";
    p.style.fontSize = "20px";
    p.innerHTML = "Processing";

    container.appendChild(p);
    document.getElementById("inputSignin").parentElement.submit();
}


