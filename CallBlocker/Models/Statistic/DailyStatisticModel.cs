﻿using CallBlocker.Models.Enums;
using System;

namespace CallBlocker.Models.Statistic
{
    public class DailyStatisticModel
    {
        public DateTimeOffset DateTime { get; set; }

        public int Count { get; set; }

        public StatisticType Type { get; set; }
    }
}
