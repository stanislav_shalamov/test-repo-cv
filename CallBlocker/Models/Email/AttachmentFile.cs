﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Email
{
    public class AttachmentFile
    {
        public string Base64Content { get; set; }
        public string FileName { get; set; }
    }
}
