﻿namespace CallBlocker.Models
{
    public class EmailSettings
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string[] DeveloperEmails { get; set; }
        public string[] DevStatisticSubscribers { get; set; }
    }
}
