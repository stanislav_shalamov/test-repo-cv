﻿namespace CallBlocker.Models
{
    public class PostWhiteListModel
    {
        public string UserId { get; set; }

        public string PhoneNumber { get; set; }

        public string Name { get; set; }
    }
}
