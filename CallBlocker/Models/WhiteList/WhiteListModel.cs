﻿using System;

namespace CallBlocker.Models
{
    public class WhiteListModel
    {
        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public DateTimeOffset DateTime { get; set; }
    }
}
