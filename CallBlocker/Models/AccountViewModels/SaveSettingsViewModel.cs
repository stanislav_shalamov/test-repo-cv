﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.AccountViewModels
{
    public class LoginWithSettingsViewModel
    {
        public string AdminEmail { get; set; }
        public string NotificationTime { get; set; }
    }
}
