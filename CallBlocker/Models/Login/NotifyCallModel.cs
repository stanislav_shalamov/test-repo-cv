﻿using System;

namespace CallBlocker.Models
{
    public class NotifyCallModel
    {
        public string UserId { get; set; }

        public string PhoneNumber { get; set; }

        public bool SIM { get; set; }

        public bool IsContact { get; set; }

        public string OperatorId { get; set; }

        public DateTimeOffset DateTime { get; set; }

        public bool NeedCheckSubscriptionStatus { get; set; }

        public string PurchaseToken { get; set; }
    }
}
