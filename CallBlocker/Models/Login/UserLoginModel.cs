﻿namespace CallBlocker.Models
{
    public class UserLoginModel
    {
        public string PhoneNumber { get; set; }

        public string Password { get; set; }
    }
}
