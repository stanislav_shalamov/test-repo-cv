﻿namespace CallBlocker.Models
{
    public class LoginResponseModel
    {
        public string UserId { get; set; }

        public string TwilioNumber { get; set; }

        public string VoiceMail { get; set; }

        public string SelfTestNumber { get; set; }
    }
}
