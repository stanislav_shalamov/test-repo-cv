﻿namespace CallBlocker.Models.Login
{
    public class IdentityCodeUserCodeMappingModel
    {
        public string IdentityCode { get; set; }

        public string UserCode { get; set; }
    }
}
