﻿namespace CallBlocker.Models.Login
{
    public class UserEmailLoginModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
