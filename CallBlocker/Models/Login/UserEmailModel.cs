﻿namespace CallBlocker.Models.Login
{
    public class UserEmailModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string PhoneNumber { get; set; }

        public string VoiceMail { get; set; }

        public string PhoneNumber2 { get; set; }

        public string VoiceMail2 { get; set; }
    }
}
