﻿using Microsoft.AspNetCore.Identity;
using System;

namespace CallBlocker.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string VoiceMail { get; set; }

        public string PhoneNumber2 { get; set; }

        public string VoiceMail2 { get; set; }

        public DateTimeOffset ActivityDate { get; set; }
    }
}
