﻿using System.Collections.Generic;

namespace CallBlocker.Models.Settings
{
    public class TodayStatisticSettings : ITodayStatisticSettings
    {
        public IEnumerable<string>  AdminEmails { get; set; }

        public string  AdminEmailsString { get; set; }

        public string SendTodayStatisticHours { get; set; }

        public string SendTodayStatisticMinutes { get; set; }
    }
}
