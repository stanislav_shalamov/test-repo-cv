﻿namespace CallBlocker.Models.Settings
{
    public class TwilioSettings
    {
        public string TwilioNumber { get; set; }

        public string SelfTestNumber { get; set; }

        public string Sid { get; set; }

        public string Token { get; set; }
    }
}
