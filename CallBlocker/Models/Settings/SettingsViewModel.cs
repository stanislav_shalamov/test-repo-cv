﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Settings
{
    public class SettingsViewModel
    {
        public string AdminEmails { get; set; }

        public string SendTodayStatisticHours { get; set; }

        public string SendTodayStatisticMinutes { get; set; }
    }
}
