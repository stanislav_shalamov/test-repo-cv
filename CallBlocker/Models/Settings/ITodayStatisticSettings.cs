﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Settings
{
    public interface ITodayStatisticSettings
    {
        IEnumerable<string> AdminEmails { get; set; }

        string SendTodayStatisticHours { get; set; }
    }
}
