﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;

namespace CallBlocker.Models.AccountFilters
{
    public class AccountAuthentificationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var emailCookie = context.HttpContext.Request.Cookies["CallBlockerValidationKey"];
            if (emailCookie == null)
                context.Result = new RedirectToActionResult("Authorization", "StatisticSettings", null);
        }   
    }
}
