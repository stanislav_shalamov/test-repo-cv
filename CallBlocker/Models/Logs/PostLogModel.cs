﻿namespace CallBlocker.Models.Logs
{
    public class PostLogModel
    {
        public string UserId { get; set; }

        public string Message { get; set; }
    }
}
