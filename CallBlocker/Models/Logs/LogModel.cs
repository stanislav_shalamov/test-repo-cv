﻿using System;

namespace CallBlocker.Models.Logs
{
    public class LogModel
    {
        public string UserId { get; set; }

        public string Message { get; set; }

        public DateTimeOffset DateTime { get; set; }
    }
}
