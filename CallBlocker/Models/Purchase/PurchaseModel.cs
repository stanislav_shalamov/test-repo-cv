﻿using CallBlocker.Models.Enums;

namespace CallBlocker.Models.Purchase
{
    public class PurchaseModel
    {
        public string Token { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; } //subscriptionId = annual or munthly
    }
}
