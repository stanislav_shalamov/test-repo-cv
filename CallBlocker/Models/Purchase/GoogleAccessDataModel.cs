﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class GoogleAccessDataModel
    {
        public string Access_token { get; set; }
        public string Expires_in { get; set; }
        public string Scope { get; set; }
        public string Token_type { get; set; }
    }
}
