﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class PurchaseLogModel
    {
        public string Response{ get; set; }
        public DateTimeOffset DateTime { get; set; }
        public string Message{ get; set; }
    }
}
