﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class PubSubEncodedModel
    {
        public Message Message { get; set; }
        public string Subscription { get; set; }
    }

    public class Message
    {
        public string Data { get; set; }
    }

    public class SubscriptionNotification {
        public string Version { get; set; }
        public int NotificationType { get; set; }
        public string PurchaseToken { get; set; }
        public string SubscriptionId { get; set; }
    }

    public class PubSubDecodedModel
    {
        public string Version { get; set; }
        public string PackageName { get; set; }
        public string EventTimeMillis { get; set; }
        public SubscriptionNotification SubscriptionNotification { get; set; }
    }
}
