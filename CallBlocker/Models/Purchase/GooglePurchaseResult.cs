﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class GoogleRequestResult
    {
        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public string JsonException { get; set; }
        public string OrderId { get; set; }
        public string JsonResponse { get; set; }
    }
}
