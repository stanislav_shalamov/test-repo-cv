﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class PurchaseSettings
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string PackageName { get; set; }
        public string ProductId { get; set; }
        public string RedirectURI { get; set; }
    }
}
