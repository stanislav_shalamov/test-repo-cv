﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class SubscriptionModel
    {
        public string UserId { get; set; }
        public string PurchaseToken { get; set; }
        public int SubscriptionStatus { get; set; }
        public string OrderId { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public string Email { get; set; }
    }
}
