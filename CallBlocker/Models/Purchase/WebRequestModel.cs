﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class WebRequestBuilder
    {
        private WebRequest request;

        public WebRequestBuilder(string url, string data = "")
        {
            request = WebRequest.Create(url);

            request.Method = "POST";
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
        }

        public WebRequestBuilder(string url)
        {
            request = WebRequest.Create(url);
        }

        public async Task<ResponseModel> GetResponse()
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                WebResponse response = await request.GetResponseAsync();
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        responseModel.JsonResponse = reader.ReadToEnd();
                        reader.Close();
                        dataStream.Close();
                        response.Close();
                        responseModel.IsSuccessful = true;
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse responseFromApi = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)responseFromApi;

                    using (Stream data = httpResponse.GetResponseStream())
                    {
                        responseModel.JsonError = new StreamReader(data).ReadToEnd();
                        responseModel.IsSuccessful = false;
                    }
                }
            }
            catch (Exception e)
            {
                responseModel.ErrorMessage = e.Message;
                responseModel.IsSuccessful = false;
            }
            return responseModel;
        }
    }
}
