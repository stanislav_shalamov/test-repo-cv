﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Purchase
{
    public class ResponseModel
    {
        public bool IsSuccessful { get; set; }

        public string JsonResponse { get; set; }

        public string JsonError { get; set; }

        public string ErrorMessage { get; set; }
    }
}
