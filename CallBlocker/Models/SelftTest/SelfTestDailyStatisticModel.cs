﻿using System.Collections.Generic;

namespace CallBlocker.Models.SelftTest
{
    public class SelfTestDailyStatisticModel
    {
        public SelfTestDailyStatisticModel(int failedReports, int passedReports, int conditionalyPassed, string userEmails)
        {
            FailedReports = failedReports;
            PassedReports = passedReports;
            ConditionalyPassed = conditionalyPassed;
            UserEmails = userEmails;
        }

        public int FailedReports { get; }

        public int PassedReports { get; }

        public int ConditionalyPassed { get; set; }

        public string UserEmails { get; set; }
    }
}
