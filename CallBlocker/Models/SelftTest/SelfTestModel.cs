﻿using System;

namespace CallBlocker.Models.SelftTest
{
    public class SelfTestModel
    {
        public DateTimeOffset DateTime { get; set; }

        public string UserId { get; set; }

        public string Status { get; set; }

        public string AppVersion { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public int Os { get; set; }

        public string OperatorId { get; set; }

        public string Message { get; set; }

        public string Email { get; set; }
    }
}
