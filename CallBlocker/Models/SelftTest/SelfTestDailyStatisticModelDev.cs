﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.SelftTest
{
    public class SelfTestDailyStatisticModelDev<T> where T: class
    {
        public SelfTestDailyStatisticModelDev(IEnumerable<T> failedReports, IEnumerable<T> conditionalyPassed)
        {
            FailedReports = failedReports;
            ConditionalyPassed = conditionalyPassed;
        }

        public IEnumerable<T> FailedReports { get; }
        public IEnumerable<T> ConditionalyPassed { get; set; }
    }
}
