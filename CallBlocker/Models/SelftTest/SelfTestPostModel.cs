﻿namespace CallBlocker.Models.SelftTest
{
    public class SelfTestPostModel
    {
        public string UserId { get; set; }

        public string Status { get; set; }

        public string AppVersion { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public int Os { get; set; }

        public string OperatorId { get; set; }

        public string Message { get; set; }
    }
}
