﻿namespace CallBlocker.Models.SelftTest
{
    public class SelfTestStatisticModel
    {
        public string AppVersion { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public int Os { get; set; }
    }
}
