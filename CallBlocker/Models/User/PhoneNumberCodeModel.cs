﻿namespace CallBlocker.Models.User
{
    public class PhoneNumberCodeModel
    {
        public string PhoneNumber { get; set; }

        public int Code { get; set; }
    }
}
