﻿namespace CallBlocker.Models.Enums
{
    public enum ReportReason
    {
        Business_solicitation = 1,
        Political_campaign = 2,
        Scam = 3,
        Robocall = 4,
        Survey = 5,
        Collection_agency = 6
    }
}
