﻿namespace CallBlocker.Models.Enums
{
    public enum SelfTestStatus
    {
        Passed = 1,
        Failed = 2,
        Conditionally_passed = 3
    }
}
