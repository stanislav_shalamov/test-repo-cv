﻿namespace CallBlocker.Models.Enums
{
    public enum Status
    {
        Required,
        Passed,
        Failed
    }
}
