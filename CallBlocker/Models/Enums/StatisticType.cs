﻿namespace CallBlocker.Models.Enums
{
    public enum StatisticType
    {
        ForwardedCalls = 1,
        VerifiedCalls = 2,
        BlockedCalls = 3,
        NewVerified = 4,
        FailedByExpirationTime = 5,
        FailedByWrondCode = 6
    }
}
