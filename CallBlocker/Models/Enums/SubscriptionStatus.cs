﻿namespace CallBlocker.Models.Enums
{
    public enum SubscriptionStatus
    {
        Subscription_default = 0,
        Subscription_recovered = 1,
        Subscription_renewed = 2,
        Subscription_canceled = 3,
        Subscription_purchased = 4,
        Subscription_on_hold = 5,
        Subscription_in_grace_period = 6,
        Subscription_restarted = 7,
        Subscription_price_change_confirmed = 8,
        Subscription_deferred = 9,
        Subscription_revoked = 12,
        Subscription_expired = 13
    }
}
