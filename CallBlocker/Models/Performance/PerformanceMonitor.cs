﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Models.Performance
{
    public class PerformanceMonitor
    {
        public string Operation { get; set; }
        public TimeSpan Elapsed { get; set; }
    }
}
