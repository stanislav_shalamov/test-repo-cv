﻿namespace CallBlocker.Models.BlackList
{
    public class ReportModel
    {
        public string UserId { get; set; }

        public string PhoneNumber { get; set; }
    }
}
