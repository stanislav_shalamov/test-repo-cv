﻿using System;

namespace CallBlocker.Models.BlackList
{
    public class BlackListModel
    {
        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public DateTimeOffset DateTime { get; set; }

        public string Reason { get; set; }

        public string Notes { get; set; }
    }
}
