﻿namespace CallBlocker.Models.BlackList
{
    public class ReportDailyStatiticModel
    {
        public ReportDailyStatiticModel(int businessSolicitation, int politicalCampaign, int scam, int robocall, int survey, int collectionAgency)
        {
            BusinessSolicitation = businessSolicitation;
            PoliticalCampaign = politicalCampaign;
            Scam = scam;
            Robocall = robocall;
            Survey = survey;
            CollectionAgency = collectionAgency;
        }

        public int BusinessSolicitation { get; }

        public int PoliticalCampaign { get; }

        public int Scam { get; set; }

        public int Robocall { get; set; }

        public int Survey { get; set; }

        public int CollectionAgency { get; set; }
    }
}
