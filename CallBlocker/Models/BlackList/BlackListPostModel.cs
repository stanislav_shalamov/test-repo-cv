﻿namespace CallBlocker.Models
{
    public class BlackListPostModel
    {
        public string UserId { get; set; }
        
        public string PhoneNumber { get; set; }

        public string Reason { get; set; }

        public string Notes { get; set; }

        public string Name { get; set; }
    }
}
