﻿namespace CallBlocker.Models
{
    public class FailVerificationModel
    {
        public string PhoneNumber { get; set; }

        public int Count { get; set; }
    }
}
