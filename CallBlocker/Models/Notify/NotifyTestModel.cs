﻿namespace CallBlocker.Models.Notify
{
    public class NotifyTestModel
    {
        public string UserId { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
