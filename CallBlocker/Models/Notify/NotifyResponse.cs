﻿using CallBlocker.Models.Enums;

namespace CallBlocker.Models.Notify
{
    public class NotifyResponse<T> where T : class
    {
        public Status VerificationStatus { get; set; }

        public int SubscriptionStatus { get; set; }

        public T AdditionalInfo { get; set; } 
    }

    public class OldNotifyResponse<T> where T : class
    {
        public Status VerificationStatus { get; set; }

        public T AdditionalInfo { get; set; }
    }
}
