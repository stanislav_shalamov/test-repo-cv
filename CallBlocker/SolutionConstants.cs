﻿namespace CallBlocker
{
    public static class SolutionConstants
    {
        public const string PhoneRegex = @"^[0-9+][0-9]{4,13}$";
        public const string WrongPassword = "Wrong password";
        public const string UserNotExists = "User with this email not exists";
        public const string PhoneNumberEmpty = "Phone Number is empty";
        public const string PhoneNumberWrongFormat = "Phone Number format is wrong";
        public const string WrondInputData = "Entered data is wrong";
        public const string WrongPassOrEmail = "Password or email is wrong";
        public const string WrongNewPass = "Cannot change password. Please check your new password.";
        public const string DontHaveAccess = "You can`t change password of this account";
        public const string UserAlreadyExists = "User with this email already exists";

        public const int ScamCallPercent = 80;
        public const int NumberInLocalBlackLists = 30;
        public const int IncommingCallLifeTime = 45;
        public const int QueueFailVerification = 15;

        public const string GetAccessTokenURL = "https://accounts.google.com/o/oauth2/token";
    }
}
