﻿using CallBlocker.Quartz;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;

namespace CallBlocker
{
    public static class QuartzExtension
    {
        public static void UseQuartz(this IApplicationBuilder app)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            scheduler.Start().Wait();

            ScheduleJobs(scheduler);
        }

        public static void AddQuartz(this IServiceCollection services)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
            var jobFactory = new IoCJobFactory(services.BuildServiceProvider());

            scheduler.JobFactory = jobFactory;
            services.AddSingleton(scheduler);
        }

        private static void ScheduleJobs(IScheduler scheduler)
        {
            //// delete old blobs every 10 days
            QuartzServicesUtilities.StartJob<DeleteExpiredBlobsJob>(scheduler);

            //// create blobs every 23:59 Arizona Time
            QuartzServicesUtilities.StartMidnightJob<CreateBlobJob>(scheduler);

            //// create blobs every 11:59am
            //QuartzServicesUtilities.StartMiddayJob<CreateBlobJob>(scheduler);

            //// send email with statistic every 23:55 Arizona time
            QuartzServicesUtilities.StartEverydayStatisticJob<SendTodaysStatisticJob>(scheduler);

            // send email with statistic for developers 23:45  Arizona time
            QuartzServicesUtilities.StartEverydayDeveloperStatisticJob<SendTodaysDeveloperStatisticJob>(scheduler);
        }
    }  
}
