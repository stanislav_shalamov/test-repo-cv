﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CallBlocker.Data;
using CallBlocker.Models;
using CallBlocker.Services;
using CallBlocker.Repositories.User;
using CallBlocker.Repositories.WhiteList;
using CallBlocker.Repositories.BlackList;
using CallBlocker.Models.Settings;
using CallBlocker.Repositories.Logs;
using Swashbuckle.AspNetCore.Swagger;
using CallBlocker.Repositories.SelfTest;
using CallBlocker.BlobStorage;
using CallBlocker.Quartz;
using CallBlocker.Repositories.Statistic;
using CallBlocker.Repositories.Account;
using CallBlocker.Repositories.Purchase;
using CallBlocker.Models.Purchase;
using CallBlocker.Repositories.DevLogs;

namespace CallBlocker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddTokenProvider<DataProtectorTokenProvider<ApplicationUser>>("ResetPasswordProvider");

            string connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddTransient<IUserRepository, UserRepository>(provider => new UserRepository(connectionString));
            services.AddTransient<IWhiteListRepository, WhiteListRepository>(provider => new WhiteListRepository(connectionString));
            services.AddTransient<IBlackListRepository, BlackListRepository>(provider => new BlackListRepository(connectionString));
            services.AddTransient<ILogRepository, LogRepository>(provider => new LogRepository(connectionString));
            services.AddTransient<ISelfTestRepository, SelfTestRepository>(provider => new SelfTestRepository(connectionString));
            services.AddTransient<IAccountRepository, AccountRepository>(provider => new AccountRepository(connectionString));
            services.AddTransient<IDevLogRepository, DevLogRepository>(provider => new DevLogRepository(connectionString));
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IStatisticService, StatisticService>();
            services.AddTransient<IPurchaseService, GooglePurchaseService>();
            services.AddTransient<ISettingsService, SettingsService>();
            services.AddTransient<IScamService, ScamService>();
            services.AddTransient<ISubscriptionService, SubscriptionService>();
            services.AddTransient<IDeveloperStatisticService, DeveloperStatisticService>();
            services.AddTransient<IStatisticRepository, StatisticRepository>(provider => new StatisticRepository(connectionString));

            services.Configure<TwilioSettings>(Configuration.GetSection("TwilioSettings"));
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<PurchaseSettings>(Configuration.GetSection("PurchaseSettings"));

            services.AddScoped<DeleteExpiredBlobsJob>();
            services.AddScoped<CreateBlobJob>();
            services.AddScoped<SendTodaysStatisticJob>();
            services.AddScoped<SendTodaysDeveloperStatisticJob>();

            services.AddTransient<IBlobStorageService, AzureBlobStorageService>(
                provider => new AzureBlobStorageService(Configuration.GetConnectionString("BlobStorageConnection"), "callverifycontainer"));

            services.AddTransient<IPurchaseRepository, PurchaseRepository>(provider => new PurchaseRepository(connectionString));


            services.AddMvc();
            services.AddQuartz();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "CallBlocker API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc();

            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "CallBlocker API");
            //});

            app.UseQuartz();
        }
    }
}
