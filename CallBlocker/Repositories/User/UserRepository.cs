﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.Login;
using CallBlocker.Models.Notify;
using CallBlocker.Models.User;
using Dapper;

namespace CallBlocker.Repositories.User
{
    public class UserRepository : IUserRepository
    {
        private readonly string _connectionString;

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<string> AddNewUser(string phoneNumber, string password)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                password = SolutionExtensions.CreateMD5(password);
                var sqlQuery = "exec [dbo].[AddNewUser] @phoneNumber, @password";
                return await db.QueryFirstOrDefaultAsync<string>(sqlQuery, new { phoneNumber, password });
            }
        }

        public async Task DeleteIncomingCall(string phoneNumber)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Substring(1);

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[IncomingCall] WHERE PhoneNumber LIKE CONCAT('%',@phoneNumber,'%')";
                await db.ExecuteAsync(sqlQuery, new { phoneNumber });
            }
        }

        public async Task DeleteIncomingCallByUserId(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[IncomingCall] WHERE UserId = @userId";
                await db.ExecuteAsync(sqlQuery, new { userId });
            }
        }

        public async Task<NotifyCallModel> GetIncomingCall(string phoneNumber)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Substring(1);

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[IncomingCall] WHERE PhoneNumber LIKE CONCAT('%',@phoneNumber,'%')";
                var incommingCalls = await db.QueryAsync<NotifyCallModel>(sqlQuery, new { phoneNumber });
                return incommingCalls.LastOrDefault();
            }
        }

        public async Task<string> GetUserIdByPhoneNumber(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT Id FROM [dbo].[User] where PhoneNumber = @phoneNumber";
                return await db.QueryFirstOrDefaultAsync<string>(sqlQuery, new { phoneNumber });
            }
        }

        public async Task<string> GetUserPhoneNumber(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[GetUserPhoneNumber] @userId";
                return await db.QueryFirstOrDefaultAsync<string>(sqlQuery, new { userId });
            }
        }

        public async Task<string> GetUserPhoneNumberFromIdentity(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT PhoneNumber FROM [dbo].[AspNetUsers] where Id = @userId";
                return await db.QueryFirstOrDefaultAsync<string>(sqlQuery, new { userId });
            }
        }

        public async Task<string> GetUserVoiceMail(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT VoiceMail FROM [dbo].[AspNetUsers] where Id = @userId";
                return await db.QueryFirstOrDefaultAsync<string>(sqlQuery, new { userId });
            }
        }

        public async Task AddPhoneNumberCode(string phoneNumber, int code)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"insert into [dbo].[UserCode] values(@phoneNumber, @code)";
                await db.ExecuteAsync(sqlQuery, new
                {
                    phoneNumber,
                    code
                });
            }
        }

        public async Task<PhoneNumberCodeModel> GetPhoneNumberCodes(string phoneNumber)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Substring(1);

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[UserCode] where PhoneNumber LIKE CONCAT('%',@phoneNumber,'%')";
                var codes = await db.QueryAsync<PhoneNumberCodeModel>(sqlQuery, new { phoneNumber });
                return codes.LastOrDefault();
            }
        }

        public async Task DeletePhoneNumberCode(string phoneNumber)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Substring(1);

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[UserCode] where PhoneNumber LIKE CONCAT('%',@phoneNumber,'%')";
                await db.ExecuteAsync(sqlQuery, new { phoneNumber });
            }
        }

        public async Task CreateIdentityCodeUserCodeMapping(string identityCode, string userCode, string email)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"insert into [dbo].[IdentityCodeUserCodeMapping] values(@identityCode, @userCode, @email)";
                await db.ExecuteAsync(sqlQuery, new
                {
                    identityCode,
                    userCode,
                    email
                });
            }
        }

        public async Task<IEnumerable<IdentityCodeUserCodeMappingModel>> GeIdentityCodeUserCodeMapping(string userCode, string email)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[IdentityCodeUserCodeMapping] where UserCode = @userCode and Email = @email";
                return await db.QueryAsync<IdentityCodeUserCodeMappingModel>(sqlQuery, new { userCode, email });
            }
        }

        public async Task DeleteIdentityCodeUserCodeMapping(string email)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[IdentityCodeUserCodeMapping] where Email = @email";
                await db.ExecuteAsync(sqlQuery, new { email });
            }
        }

        public async Task AddIncomingCall(string userId, string phoneNumber, bool sim, bool isContact, string operatorId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var dateTime = DateTimeOffset.UtcNow;
                var sqlQuery = "insert into [dbo].[IncomingCall] values(@userId, @phoneNumber, @sim, @isContact, @operatorId, @dateTime)";
                await db.ExecuteAsync(sqlQuery, new { userId, phoneNumber, sim, isContact, operatorId, dateTime });
            }
        }

        public async Task<IEnumerable<NotifyTestModel>> GetNotifyCall()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT i.UserId, a.Email, i.PhoneNumber FROM [dbo].[IncomingCall] i INNER JOIN [dbo].[AspNetUsers] a on i.UserId = a.Id";
                return await db.QueryAsync<NotifyTestModel>(sqlQuery);
            }
        }

        public async Task UpdateActivityDateById(string userId)
        {
            var dateTime = DateTimeOffset.UtcNow;
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "UPDATE [dbo].[AspNetUsers] SET [ActivityDate] = @dateTime WHERE [Id] = @userId";
                await db.ExecuteAsync(sqlQuery, new { dateTime, userId });
            }
        }

        public async Task<int> GetUsersCountByDate(DateTime date)
        {
            var parsedDate = date.ToString("yyyy-MM-dd");

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT COUNT(*) FROM [dbo].[AspNetUsers] WHERE [ActivityDate] > @parsedDate";
                return await db.QueryFirstOrDefaultAsync<int>(sqlQuery, new { parsedDate });
            }
        }
    }
}
