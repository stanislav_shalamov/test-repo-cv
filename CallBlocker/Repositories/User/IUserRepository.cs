﻿using System.Collections.Generic;
using CallBlocker.Models;
using CallBlocker.Models.Login;
using System.Threading.Tasks;
using CallBlocker.Models.Notify;
using CallBlocker.Models.User;
using System;

namespace CallBlocker.Repositories.User
{
    public interface IUserRepository
    {
        Task<string> AddNewUser(string phoneNumber, string password);

        Task<string> GetUserIdByPhoneNumber(string phoneNumber);

        Task DeleteIncomingCall(string phoneNumber);

        Task<NotifyCallModel> GetIncomingCall(string phoneNumber);

        Task<string> GetUserPhoneNumber(string userId);

        Task CreateIdentityCodeUserCodeMapping(string identityCode, string userCode, string email);

        Task<IEnumerable<IdentityCodeUserCodeMappingModel>> GeIdentityCodeUserCodeMapping(string userCode, string email);

        Task DeleteIdentityCodeUserCodeMapping(string email);

        Task AddIncomingCall(string userId, string phoneNumber, bool sim, bool isContact, string operatorId);

        Task<string> GetUserPhoneNumberFromIdentity(string userId);

        Task<string> GetUserVoiceMail(string userId);

        Task AddPhoneNumberCode(string phoneNumber, int code);

        Task<PhoneNumberCodeModel> GetPhoneNumberCodes(string phoneNumber);

        Task DeletePhoneNumberCode(string phoneNumber);

        Task<IEnumerable<NotifyTestModel>> GetNotifyCall();

        Task DeleteIncomingCallByUserId(string userId);

        Task UpdateActivityDateById(string userId);

        Task<int> GetUsersCountByDate(DateTime date);
    }
}
