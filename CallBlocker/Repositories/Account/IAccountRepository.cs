﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.Account
{
    public interface IAccountRepository
    {
        Task<bool> EmailIsValid(string email);

        Task<IEnumerable<string>> GetAdminEmails();

        Task SaveAdminEmails(IEnumerable<string> adminEmails);
    }
}
