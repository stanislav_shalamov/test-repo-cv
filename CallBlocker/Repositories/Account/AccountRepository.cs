﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.Account
{
    public class AccountRepository : IAccountRepository
    {
        private readonly string _connectionString;

        public AccountRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<bool> EmailIsValid(string email)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT CASE WHEN EXISTS (SELECT * FROM [dbo].[AspNetAdminEmail] WHERE [Email] = @email) THEN 'TRUE' ELSE 'FALSE' END";
                return await db.QueryFirstOrDefaultAsync<bool>(sqlQuery, new { email });
            }
        }

        public async Task<IEnumerable<string>> GetAdminEmails()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT [Email] FROM [dbo].[AspNetAdminEmail]";
                return await db.QueryAsync<string>(sqlQuery);
            }
        }

        public async Task SaveAdminEmails(IEnumerable<string> adminEmails)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQueryDeleteTable = @"DELETE FROM [dbo].[AspNetAdminEmail]";
                await db.ExecuteAsync(sqlQueryDeleteTable);

                foreach (var adminEmail in adminEmails)
                {
                    var sqlQueryInsert = @"INSERT INTO [dbo].[AspNetAdminEmail] VALUES (@adminEmail)";
                    await db.ExecuteAsync(sqlQueryInsert, new { @adminEmail });
                }
            }
        }
    }
}
