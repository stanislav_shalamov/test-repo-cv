﻿using CallBlocker.Models.Enums;
using CallBlocker.Models.Purchase;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.Purchase
{
    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly string _connectionString;

        public PurchaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<bool> PurchaseAlreadyExist(string userId, string purchaseToken)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT CASE WHEN EXISTS (SELECT * FROM [dbo].[Subscription] WHERE [UserId] = @userId AND [PurchaseToken] = @purchaseToken) THEN 'TRUE' ELSE 'FALSE' END";
                return await db.QueryFirstOrDefaultAsync<bool>(sqlQuery, new { userId, purchaseToken });
            }
        }

        public async Task<bool> OrderAlreadyExist(string orderId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT CASE WHEN EXISTS (SELECT * FROM [dbo].[Subscription] WHERE [OrderId] = @orderId) THEN 'TRUE' ELSE 'FALSE' END";
                return await db.QueryFirstOrDefaultAsync<bool>(sqlQuery, new { orderId });
            }
        }

        public async Task Add(string userId, string token, int subscriptionStatus, string orderId)
        {
            DateTime dateTime = DateTime.UtcNow;
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"INSERT INTO [dbo].[Subscription] VALUES (@userId, @token, @subscriptionStatus, @orderId, @dateTime)";
                await db.ExecuteAsync(sqlQuery, new { userId, token, subscriptionStatus, orderId, dateTime });
            }
        }

        public async Task Update(int notificationType, string orderId)
        {
            DateTime dateTime = DateTime.UtcNow;
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"UPDATE [dbo].[Subscription] SET [SubscriptionStatus] = @notificationType, [DateTime] = @dateTime WHERE [OrderId] = @orderId";
                await db.ExecuteAsync(sqlQuery, new { notificationType, dateTime, orderId });
            }
        }

        public async Task CreateAuthCode(GoogleAccessModel model)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"INSERT INTO [dbo].[OAuthCode] VALUES (@access_token, @expires_in, @refresh_token, GETDATE())";
                await db.ExecuteAsync(sqlQuery, new
                {
                    model.Access_token,
                    model.Expires_in,
                    model.Refresh_token,
                });
            }
        }

        public async Task<int> GetSubscriptionStatus(string userId, string purchaseToken)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                //var sqlQuery = @"SELECT [SubscriptionStatus] FROM [dbo].[Subscription] WHERE [UserId] = @userId";
                var sqlQuery = @"SELECT [SubscriptionStatus] FROM [dbo].[Subscription] WHERE [UserId] = @userId AND [PurchaseToken] = @purchaseToken";
                return await db.QueryFirstOrDefaultAsync<int>(sqlQuery, new { userId, purchaseToken });
            }
        }

        public async Task<string> GetRefreshToken()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT [Refresh_Token] FROM [dbo].[OAuthCode] ORDER BY [DateTime] DESC";
                return await db.QueryFirstOrDefaultAsync<string>(sqlQuery);
            }
        }

        public async Task CreateLogs(string response, string message)
        {
            var dateTime = DateTime.UtcNow;
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"INSERT INTO [dbo].[GooglePurchaseResponseLog] VALUES (@response, @dateTime, @message)";
                await db.ExecuteAsync(sqlQuery, new { response, dateTime, message });
            }
        }

        public async Task<IEnumerable<PurchaseLogModel>> GetLogs()
        {
            var dateTime = DateTime.UtcNow;
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT * FROM [dbo].[GooglePurchaseResponseLog]";
                return await db.QueryAsync<PurchaseLogModel>(sqlQuery);
            }
        }

        public async Task OldUserLog(string userId)
        {
            var dateTime = DateTime.UtcNow;
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"INSERT INTO [dbo].[NotifyCallV1Log] VALUES (@userId, @dateTime)";
                await db.ExecuteAsync(sqlQuery, new { userId, dateTime });
            }
        }

        public async Task<IEnumerable<SubscriptionModel>> Get()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                //var sqlQuery = @"SELECT * FROM [dbo].[Subscription]";
                var sqlQuery = $"SELECT [UserId], [PurchaseToken], [SubscriptionStatus], [OrderId], [DateTime], [Email] FROM [dbo].[Subscription] " +
                    $"JOIN [dbo].[AspNetUsers] ON [dbo].[AspNetUsers].[Id] = [dbo].[Subscription].[UserId]";
                return await db.QueryAsync<SubscriptionModel>(sqlQuery);
            }
        }
    }
}
