﻿using CallBlocker.Models.Enums;
using CallBlocker.Models.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.Purchase
{
    public interface IPurchaseRepository
    {
        Task<bool> PurchaseAlreadyExist(string userId, string purchaseToken);

        Task Add(string userId, string token, int notificationType, string orderId);

        Task<int> GetSubscriptionStatus(string userId, string purchaseToken);

        Task CreateAuthCode(GoogleAccessModel model);

        Task<string> GetRefreshToken();

        Task CreateLogs(string response, string message);

        Task<IEnumerable<PurchaseLogModel>> GetLogs();

        Task OldUserLog(string userId);

        Task<bool> OrderAlreadyExist(string orderId);

        Task Update(int notificationType, string orderId);

        Task<IEnumerable<SubscriptionModel>> Get();
    }
}
