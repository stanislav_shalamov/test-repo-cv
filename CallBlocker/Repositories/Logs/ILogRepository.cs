﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CallBlocker.Models.Logs;

namespace CallBlocker.Repositories.Logs
{
    public interface ILogRepository
    {
        Task Create(PostLogModel model);

        Task Delete(string userId);

        Task<IEnumerable<LogModel>> Get(string userId);

        Task<IEnumerable<LogModel>> GetByEmail(string email);

        Task<IEnumerable<LogModel>> GetLogs();

        Task DeleteTodaysLogs(DateTime date);
    }
}
