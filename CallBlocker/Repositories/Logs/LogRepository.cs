﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using CallBlocker.Models.Logs;
using Dapper;

namespace CallBlocker.Repositories.Logs
{
    public class LogRepository : ILogRepository
    {
        private readonly string _connectionString;

        public LogRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Create(PostLogModel model)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var dateTime = DateTimeOffset.UtcNow;
                var sqlQuery = "insert into [dbo].[Log] values(@userId, @message, @dateTime)";
                await db.ExecuteAsync(sqlQuery, new { model.UserId, model.Message, dateTime });
            }
        }

        public async Task Delete(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[Log] WHERE UserId = @userId";
                await db.ExecuteAsync(sqlQuery, new { userId });
            }
        }

        public async Task<IEnumerable<LogModel>> Get(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[Log] WHERE UserId = @userId";
                return await db.QueryAsync<LogModel>(sqlQuery, new { userId });
            }
        }

        public async Task<IEnumerable<LogModel>> GetByEmail(string email)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT l.* FROM [dbo].[Log] l INNER JOIN [dbo].[AspNetUsers] u ON l.UserId = u.Id " +
                               $"WHERE u.Email = @email";
                return await db.QueryAsync<LogModel>(sqlQuery, new { email });
            }
        }

        public async Task<IEnumerable<LogModel>> GetLogs()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[Log]";
                return await db.QueryAsync<LogModel>(sqlQuery); ;
            }
        }

        public async Task DeleteTodaysLogs(DateTime date)
        {
            var parsedDate = date.ToString("yyyy-MM-dd");

            using (var db = new SqlConnection(_connectionString))
            {
               // var sqlQuery = "DELETE FROM [dbo].[Log] WHERE [DateTime] like CONCAT('%',@parsedDate,'%') and [DateTime] < @date";
                var sqlQuery = "TRUNCATE TABLE [dbo].[Log]";
                await db.ExecuteAsync(sqlQuery);
            }
        }
    }
}
