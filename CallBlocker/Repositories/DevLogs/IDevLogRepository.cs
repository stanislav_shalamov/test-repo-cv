﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.DevLogs
{
    public interface IDevLogRepository
    {
        Task Add(DevLogModel model);
        Task<IEnumerable<DevLogModel>> Get();
        Task Delete();
    }

    public class DevLogRepository : IDevLogRepository
    {
        private readonly string _connectionString;

        public DevLogRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(DevLogModel model)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var dateTime = DateTimeOffset.UtcNow;
                var sqlQuery = "insert into [dbo].[DevLog] values(@event, @message, @dateTime)";
                await db.ExecuteAsync(sqlQuery, new { model.Event, model.Message, model.DateTime});
            }
        }

        public async Task<IEnumerable<DevLogModel>> Get()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var dateTime = DateTimeOffset.UtcNow;
                var sqlQuery = "SELECT * FROM [dbo].[DevLog]";
                return await db.QueryAsync<DevLogModel>(sqlQuery);
            }
        }

        public async Task Delete()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var dateTime = DateTimeOffset.UtcNow;
                var sqlQuery = "DELETE [dbo].[DevLog]";
                await db.ExecuteAsync(sqlQuery);
            }
        }
    }

    public class DevLogModel
    {
        public string Event { get; set; }

        public string Message { get; set; }

        public DateTimeOffset DateTime { get; set; }
    }
}
