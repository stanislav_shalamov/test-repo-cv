﻿using CallBlocker.Models;
using CallBlocker.Models.BlackList;
using CallBlocker.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.BlackList
{
    public interface IBlackListRepository
    {
        Task DeleteFromBlackList(string phoneNumber);

        Task AddToBlackList(BlackListPostModel model);

        Task<IEnumerable<BlackListModel>> GetBlackList();

        Task<BlackListModel> GetNumberFromBlackList(string phoneNumber);

        Task DeleteReportFromBlackList(string userId, string phoneNumber);

        Task AddToReport(BlackListPostModel model);

        Task<IEnumerable<BlackListPostModel>> NumberInReports(string phoneNumber);

        Task<BlackListModel> GetNumberFromReports(string userId, string phoneNumber);

        Task<IEnumerable<BlackListModel>> GetReports(string userId);

        Task AddFailVerification(string phoneNumber);

        Task UpdateFailVerification(string phoneNumber, int count);

        Task<FailVerificationModel> SelectFailVerification(string phoneNumber);

        Task DeleteFailVerification(string phoneNumber);

        Task<List<BlackListModel>> GetTodaysReportsStatistic(DateTime date);
    }
}
