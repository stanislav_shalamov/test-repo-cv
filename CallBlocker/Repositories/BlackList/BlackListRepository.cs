﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.BlackList;
using CallBlocker.Models.Enums;
using Dapper;

namespace CallBlocker.Repositories.BlackList
{
    public class BlackListRepository : IBlackListRepository
    {
        private readonly string _connectionString;

        public BlackListRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task AddToBlackList(BlackListPostModel model)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[AddToBlackList] @userId, @phoneNumber, @notes, @reason, @name";
                await db.ExecuteAsync(sqlQuery, new { model.UserId, model.PhoneNumber, model.Notes, model.Reason, model.Name });
            }
        }

        public async Task AddToReport(BlackListPostModel model)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[AddToReport] @userId, @phoneNumber, @notes, @reason, @name";
                await db.ExecuteAsync(sqlQuery, new { model.UserId, model.PhoneNumber, model.Notes, model.Reason, model.Name });
            }
        }

        public async Task DeleteFromBlackList(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[BlackList] WHERE PhoneNumber = @phoneNumber";
                await db.ExecuteAsync(sqlQuery, new { phoneNumber });
            }
        }

        public async Task DeleteReportFromBlackList(string userId, string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM [dbo].[Report] WHERE UserId = @userId and PhoneNumber = @phoneNumber";
                await db.ExecuteAsync(sqlQuery, new { userId, phoneNumber });
            }
        }

        public async Task<IEnumerable<BlackListModel>> GetBlackList()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[BlackList]";
                return await db.QueryAsync<BlackListModel>(sqlQuery);
            }
        }

        public async Task<IEnumerable<BlackListModel>> GetReports(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[Report] where UserId = @userId";
                return await db.QueryAsync<BlackListModel>(sqlQuery, new { userId });
            }
        }

        public async Task<BlackListModel> GetNumberFromBlackList(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "select * from dbo.BlackList where PhoneNumber = @phoneNumber";
                return await db.QueryFirstOrDefaultAsync<BlackListModel>(sqlQuery, new { phoneNumber });
            }
        }

        public async Task<BlackListModel> GetNumberFromReports(string userId, string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "select * from dbo.Report where UserId = @userId and PhoneNumber = @phoneNumber";
                return await db.QueryFirstOrDefaultAsync<BlackListModel>(sqlQuery, new { userId, phoneNumber });
            }
        }

        public async Task<IEnumerable<BlackListPostModel>> NumberInReports(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
               // var sqlQuery = "select Reason from dbo.Report where PhoneNumber = @phoneNumber";
                var sqlQuery = "SELECT DISTINCT [UserId], [Reason] FROM [dbo].[Report] " +
                    "WHERE [UserId] IN " +
                    "(SELECT DISTINCT [UserId] FROM [dbo].[Report] WHERE [PhoneNumber] = @phoneNumber)";
                return await db.QueryAsync<BlackListPostModel>(sqlQuery, new { phoneNumber });
            }
        }

        public async Task<List<BlackListModel>> GetTodaysReportsStatistic(DateTime date)
        {        
            var parsedDate = date.ToString("yyyy-MM-dd");
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[Report] WHERE [DateTime] > @parsedDate";
                var result = await db.QueryAsync<BlackListModel>(sqlQuery, new { parsedDate });
                return result.ToList();
            }
        }

        public async Task AddFailVerification(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "insert into dbo.FailVerification values(@phoneNumber, 1)";
                await db.ExecuteAsync(sqlQuery, new { phoneNumber });
            }
        }

        public async Task DeleteFailVerification(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "delete from dbo.FailVerification where PhoneNumber = @phoneNumber";
                await db.ExecuteAsync(sqlQuery, new { phoneNumber });
            }
        }

        public async Task UpdateFailVerification(string phoneNumber, int count)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "update dbo.FailVerification set count = @count where PhoneNumber = @phoneNumber";
                await db.ExecuteAsync(sqlQuery, new { phoneNumber, count });
            }
        }

        public async Task<FailVerificationModel> SelectFailVerification(string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "select * from dbo.FailVerification where PhoneNumber = @phoneNumber";
                return await db.QueryFirstOrDefaultAsync<FailVerificationModel>(sqlQuery, new { phoneNumber });
            }
        }
    }
}
