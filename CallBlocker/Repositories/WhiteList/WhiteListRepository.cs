﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using CallBlocker.Models;
using Dapper;

namespace CallBlocker.Repositories.WhiteList
{
    public class WhiteListRepository : IWhiteListRepository
    {
        private readonly string _connectionString;

        public WhiteListRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task AddToWhiteList(string userId, string phoneNumber, string name)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[AddToWhiteList] @userId, @phoneNumber, @name";
                await db.ExecuteAsync(sqlQuery, new { userId, phoneNumber, name });
            }
        }

        public async Task<bool> CheckNumberInWhiteList(string userId, string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[CheckNumberInWhiteList] @userId, @phoneNumber";
                return await db.QueryFirstOrDefaultAsync<bool>(sqlQuery, new { userId, phoneNumber });
            }
        }

        public async Task<WhiteListModel> GetNumberFromWhiteList(string userId, string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "select * from dbo.WhiteList where UserId = @userId and PhoneNumber = @phoneNumber";
                return await db.QueryFirstOrDefaultAsync<WhiteListModel>(sqlQuery, new { userId, phoneNumber });
            }
        }

        public async Task DeleteFromWhiteList(string userId, string phoneNumber)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[DeleteFromWhiteList] @userId, @phoneNumber";
                await db.ExecuteAsync(sqlQuery, new { userId, phoneNumber });
            }
        }

        public async Task<IEnumerable<WhiteListModel>> GetWhiteList(string userId)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "exec [dbo].[GetWhiteList] @userId";
                return await db.QueryAsync<WhiteListModel>(sqlQuery, new { userId });
            }
        }
    }
}
