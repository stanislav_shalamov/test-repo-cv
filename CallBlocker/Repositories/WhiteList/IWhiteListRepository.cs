﻿using CallBlocker.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.WhiteList
{
    public interface IWhiteListRepository
    {
        Task<bool> CheckNumberInWhiteList(string userId, string phoneNumber);

        Task<IEnumerable<WhiteListModel>> GetWhiteList(string userId);

        Task AddToWhiteList(string userId, string phoneNumber, string name);

        Task DeleteFromWhiteList(string userId, string phoneNumber);

        Task<WhiteListModel> GetNumberFromWhiteList(string userId, string phoneNumber);
    }
}
