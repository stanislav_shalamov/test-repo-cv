﻿using CallBlocker.Models.SelftTest;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Linq;
using CallBlocker.Models.Enums;

namespace CallBlocker.Repositories.SelfTest
{
    public class SelfTestRepository : ISelfTestRepository
    {
        private readonly string _connectionString;

        public SelfTestRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IEnumerable<SelfTestModel>> Get()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[SelfTest]";
                return await db.QueryAsync<SelfTestModel>(sqlQuery);
            }
        }

        public async Task<IEnumerable<SelfTestStatisticModel>> GetStatistic()
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT DISTINCT AppVersion,Manufacturer,Model,OS FROM [dbo].[SelfTest] WHERE [Status] = 'passed'";
                return await db.QueryAsync<SelfTestStatisticModel>(sqlQuery);
            }
        }

        public async Task Add(SelfTestPostModel model)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"exec [dbo].[AddToSelfTest] @userId, @status, @appVersion, @manufacturer, @model, @oS, @operatorId, @message";
                await db.ExecuteAsync(sqlQuery, new
                {
                    model.UserId,
                    model.Status,
                    model.AppVersion,
                    model.Manufacturer,
                    model.Model,
                    model.Os,
                    model.OperatorId,
                    model.Message
                });
            }
        }

        public async Task<bool> IsValid(string manufacturer, string model, string os)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = @"SELECT CASE WHEN EXISTS (SELECT * FROM [dbo].[SelfTest] WHERE [Manufacturer] = @manufacturer AND [Model] = @model AND[OS] = @oS AND [Status] = 'passed') THEN 'TRUE' ELSE 'FALSE' END";
                return await db.QueryFirstOrDefaultAsync<bool>(sqlQuery, new { manufacturer, model, os });
            }
        }

        public async Task<List<SelfTestModel>> GetTodaysStatistic(DateTime date)
        {
            var parsedDate = date.ToString("yyyy-MM-dd");

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM [dbo].[SelfTest] WHERE [DateTime] > @parsedDate";
                var result = await db.QueryAsync<SelfTestModel>(sqlQuery, new { parsedDate });
                return result.ToList();
            }
        }

        public async Task<IEnumerable<SelfTestModel>> GetTodaysSelfTestStatisticDev(DateTime date)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT " +
                    "[DateTime], [UserId], [Status], [AppVersion], [Manufacturer], [Model], " +
                    "[OS], [OperatorId], [Message], [Email] FROM [dbo].[SelfTest] " +
                    "JOIN [dbo].[AspNetUsers] ON [dbo].[AspNetUsers].[Id] = [dbo].[SelfTest].[UserId] WHERE [DateTime] > @date " +
                    "AND [UserId] IN " +
                    "(SELECT DISTINCT [UserId] FROM [dbo].[SelfTest] WHERE ([Status] = 'failed' OR [Status] = 'conditionally_passed') AND [DateTime] > @date)";
                var result = await db.QueryAsync<SelfTestModel>(sqlQuery, new { date });
                return result.ToList();
            }
        }

        public async Task<IEnumerable<string>> GetUserEmailsByStatus(DateTime date, SelfTestStatus selfTestStatus)
        {
            var parsedDate = date.ToString("yyyy-MM-dd");
            var status = selfTestStatus.ToString().ToLower();

            using (var db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT DISTINCT CONCAT([Email], ' (' + [Manufacturer] , + ' ' + [Model] + ')') FROM [dbo].[SelfTest] JOIN [dbo].[AspNetUsers] ON [dbo].[SelfTest].[UserId] = [dbo].[AspNetUsers].[Id] WHERE [Status] = @status AND [DateTime] > @parsedDate";
                return await db.QueryAsync<string>(sqlQuery, new { status, parsedDate });
            }
        }
    }
}
