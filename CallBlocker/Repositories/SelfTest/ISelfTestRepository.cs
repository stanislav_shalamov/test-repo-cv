﻿using CallBlocker.Models.Enums;
using CallBlocker.Models.SelftTest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.SelfTest
{
    public interface ISelfTestRepository
    {
        Task<IEnumerable<SelfTestModel>> Get();

        Task Add(SelfTestPostModel model);

        Task<IEnumerable<SelfTestStatisticModel>> GetStatistic();

        Task<List<SelfTestModel>> GetTodaysStatistic(DateTime date);

        Task<IEnumerable<string>> GetUserEmailsByStatus(DateTime date, SelfTestStatus selfTestStatus);

        Task<bool> IsValid(string manufacturer, string model, string os);

        Task<IEnumerable<SelfTestModel>> GetTodaysSelfTestStatisticDev(DateTime date);
    }
}
