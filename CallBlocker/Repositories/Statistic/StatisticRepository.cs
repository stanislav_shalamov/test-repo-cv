﻿using CallBlocker.Models.Statistic;
using System;
using System.Threading.Tasks;
using CallBlocker.Models.Enums;
using System.Data.SqlClient;
using Dapper;

namespace CallBlocker.Repositories.Statistic
{
    public class StatisticRepository : IStatisticRepository
    {
        private readonly string _connectionString;

        public StatisticRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<DailyStatisticModel> GetDailyStatisticByType(DateTime date, StatisticType type)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var parsedDate = date.ToString("yyyy-MM-dd");
                var parsedType = (int)type;
                var sqlQuery = "SELECT * FROM [dbo].[DailyStatistic] WHERE [DateTime] like CONCAT('%',@parsedDate,'%') AND Type = @parsedType";
                return await db.QueryFirstOrDefaultAsync<DailyStatisticModel>(sqlQuery, new { parsedDate, parsedType });
            }
        }

        public async Task AddDailyStatistic(StatisticType type)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var dateTime = DateTimeOffset.UtcNow;
                var parsedType = (int)type;
                var sqlQuery = "insert into [dbo].[DailyStatistic] values(@dateTime, 1, @parsedType)";
                await db.ExecuteAsync(sqlQuery, new { dateTime, parsedType });
            }
        }

        public async Task UpdateDailyStatistic(StatisticType type)
        {
            using (var db = new SqlConnection(_connectionString))
            {
                var parsedDate = DateTimeOffset.UtcNow.ToString("yyyy-MM-dd");
                var parsedType = (int)type;
                var sqlQuery = "UPDATE [dbo].[DailyStatistic] SET Count = Count + 1 WHERE [DateTime] like CONCAT('%',@parsedDate,'%') AND Type = @parsedType";
                await db.ExecuteAsync(sqlQuery, new { parsedDate, parsedType });
            }
        }
    }
}
