﻿using CallBlocker.Models.Enums;
using CallBlocker.Models.Statistic;
using System;
using System.Threading.Tasks;

namespace CallBlocker.Repositories.Statistic
{
    public interface IStatisticRepository
    {
        Task<DailyStatisticModel> GetDailyStatisticByType(DateTime date, StatisticType type);

        Task AddDailyStatistic(StatisticType type);

        Task UpdateDailyStatistic(StatisticType type);
    }
}
