﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Repositories.DevLogs;
using CallBlocker.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace CallBlocker.Controllers.dev
{
    [Route("api/dev")]
    public class DevController : Controller
    {
        private readonly IDeveloperStatisticService _developerStatisticService;
        private readonly IDevLogRepository _devLogRepository;
        private readonly EmailSettings _emailSettings;

        public DevController(IDeveloperStatisticService developerStatisticService, IOptions<EmailSettings> emailSettings,
            IDevLogRepository devLogRepository)
        {
            _developerStatisticService = developerStatisticService;
            _emailSettings = emailSettings.Value;
            _devLogRepository = devLogRepository;
        }

        [HttpGet("statistic/{email}")]
        public IActionResult GetStatistic(string email)
        {
            if (string.IsNullOrEmpty(email) || _emailSettings.DeveloperEmails.Count(e => e.Equals(email)) == 0)
                return BadRequest();

            ViewBag.Email = email;
            return View("GetStatisticByEmail");
        }

        [HttpPost("statistic/devlogs")]
        public async Task<IActionResult> SendDevStatistic([FromBody] string email)
        {
            if (string.IsNullOrEmpty(email))
                return BadRequest();

            await _developerStatisticService.SendLogsDeveloperStatistic(new[] { email });
            return Ok("DevStatistic sent to " + email);
        }

        [HttpPost("statistic/purchaselogs")]
        public async Task<IActionResult> SendPurchaseLog([FromBody] string email)
        {
            if (string.IsNullOrEmpty(email))
                return BadRequest();

            await _developerStatisticService.SendGooglePurchaseLogsDeveloperStatistic(new[] { email });
            return Ok("Purchase logs sent to " + email);
        }



        //test

        [HttpPost("statistic/ShowDevLogs")]
        public async Task<IActionResult> ShowDevLogs()
        {
           // await _devLogRepository.Add(new DevLogModel() { Event="Test " + DateTime.UtcNow, DateTime = DateTime.UtcNow, Message = "save from code"});

            var logs = await _devLogRepository.Get();

            //await _devLogRepository.Delete();
            return Ok(logs);
        }
    }
}