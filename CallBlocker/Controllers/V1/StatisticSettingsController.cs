﻿using System;
using System.Threading.Tasks;
using CallBlocker.Models.AccountFilters;
using CallBlocker.Models.AccountViewModels;
using CallBlocker.Models.Settings;
using CallBlocker.Repositories.Account;
using CallBlocker.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CallBlocker.Controllers.V1
{
    [Produces("application/json")]
    [Route("api/Settings")]
    public class StatisticSettingsController : Controller
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ISettingsService _settingsService;

        public StatisticSettingsController(IAccountRepository accountRepository, ISettingsService settingsService)
        {
            _accountRepository = accountRepository;
            _settingsService = settingsService;
        }

        [ResponseCache(NoStore =true, Location =ResponseCacheLocation.None)]
        [AccountAuthentification]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            TodayStatisticSettings todayStatisticSettings = new TodayStatisticSettings();

            todayStatisticSettings.AdminEmails = await _accountRepository.GetAdminEmails();
            var cronExpression = await QuartzServicesUtilities.GetJobInterval();

            _settingsService.GetTriggerFireTime(cronExpression, out string hours, out string minutes);
            todayStatisticSettings.SendTodayStatisticHours = hours;
            todayStatisticSettings.SendTodayStatisticMinutes = minutes;

            return View(todayStatisticSettings);
        }

        [Route("Login")]
        [HttpGet]
        public async Task<IActionResult> Authorization()
        {
            return View();
        }

        [Route("Login")]
        [HttpPost]
        public async Task<IActionResult> AuthorizationPost(LoginWithSettingsViewModel model)
        {
            if (string.IsNullOrEmpty(model.AdminEmail))
                return View("Authorization");

            var emailIsValid = await _accountRepository.EmailIsValid(model.AdminEmail);

            if (emailIsValid == false)
            {
                ViewBag.Error = "Couldn't find your Call Verify Account";
                return View("Authorization");
            }

            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = new DateTimeOffset(DateTime.Now.AddMinutes(10));
            Response.Cookies.Append("CallBlockerValidationKey", model.AdminEmail, cookieOptions);
            return RedirectToAction("Index");
        }

        [Route("Logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            var cookie = Request.Cookies;
            if (cookie != null)
            {
                Response.Cookies.Delete("CallBlockerValidationKey");
            }
            return RedirectToAction("Authorization");
        }

        [Route("SaveSettings")]
        [AccountAuthentification]
        [HttpPost]
        public async Task<IActionResult> SaveSettings([FromBody]TodayStatisticSettings statisticModel)
        {
            var adminEmailFromCookie = Request.Cookies["CallBlockerValidationKey"];
            bool emailIsValid;
            try
            {
                emailIsValid = await _accountRepository.EmailIsValid(adminEmailFromCookie);

                await _settingsService.SaveAdminEmails(statisticModel.AdminEmailsString);
                await _settingsService.ResheduleJobWithNewTime(statisticModel.SendTodayStatisticHours, statisticModel.SendTodayStatisticMinutes);
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                ViewBag.Success = false;
                return PartialView();
            }

            if (!emailIsValid)
            {
                ViewBag.Message = "You are not Admin!";
                ViewBag.Success = false;
                return PartialView();
            }

            ViewBag.Message = "Saved!";
            ViewBag.Success = true;

            return PartialView();
        }

        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var adminEmails = await _accountRepository.GetAdminEmails();
            var cronExpression = await QuartzServicesUtilities.GetJobInterval();

            _settingsService.GetTriggerFireTime(cronExpression, out string hours, out string minutes);

            return Json(new { hours, minutes, adminEmails = string.Join(";", adminEmails) });
        }
    }
}