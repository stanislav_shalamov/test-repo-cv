﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.Enums;
using CallBlocker.Repositories.BlackList;
using CallBlocker.Repositories.DevLogs;
using CallBlocker.Repositories.Purchase;
using CallBlocker.Repositories.Statistic;
using CallBlocker.Repositories.User;
using CallBlocker.Repositories.WhiteList;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Twilio.AspNet.Core;
using Twilio.TwiML;
using Twilio.TwiML.Voice;
using Task = System.Threading.Tasks.Task;

namespace CallBlocker.Controllers.V1
{
    [Route("api/[controller]")]
    public class CallController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IWhiteListRepository _whiteListRepository;
        private readonly IBlackListRepository _blackListRepository;
        private readonly IStatisticRepository _statisticRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IPurchaseRepository _purchaseRepository;

        private readonly IDevLogRepository _devLogRepository;

        public CallController(IUserRepository userRepository, IWhiteListRepository whiteListRepository,
            IBlackListRepository blackListRepository, IStatisticRepository statisticRepository, IHostingEnvironment hostingEnvironment,
            IPurchaseRepository purchaseRepository, IDevLogRepository devLogRepository)
        {
            _userRepository = userRepository;
            _whiteListRepository = whiteListRepository;
            _blackListRepository = blackListRepository;
            _statisticRepository = statisticRepository;
            _hostingEnvironment = hostingEnvironment;
            _purchaseRepository = purchaseRepository;
            _devLogRepository = devLogRepository;
        }

        [HttpPost]
        public async Task<TwiMLResult> Post(string from)
        {
            var response = new VoiceResponse();
            var parsedPhoneNumber = SolutionExtensions.ParsePhoneNumber(from);
            var incomingCall = await _userRepository.GetIncomingCall(parsedPhoneNumber);


            await _devLogRepository.Add(new DevLogModel()
            { Event = "api/Call/Post ", Message = $"[from] = {from}; [parsedPhoneNumber] = {parsedPhoneNumber}; [incomingCallmodel] = " + JsonConvert.SerializeObject(incomingCall), DateTime = DateTime.UtcNow });

            if (incomingCall != null)
            {
                #region Collect statistics
                var forwardedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.ForwardedCalls);

                if (forwardedCallsStatistic == null)
                    await _statisticRepository.AddDailyStatistic(StatisticType.ForwardedCalls);
                else
                    await _statisticRepository.UpdateDailyStatistic(StatisticType.ForwardedCalls);
                #endregion

                if (incomingCall.DateTime.AddSeconds(SolutionConstants.IncommingCallLifeTime) < DateTimeOffset.UtcNow)
                    return await GetResponseNumberInBlackList(from, response);

                if (incomingCall.IsContact)
                    return !string.IsNullOrEmpty(incomingCall.OperatorId)
                        ? await GetResponseNumberDiffOperator(from, response)
                        : await GetResponseNumberInWhiteList(from, response);

                var numberInWhiteList = await _whiteListRepository.GetNumberFromWhiteList(incomingCall.UserId, incomingCall.PhoneNumber);
                if (numberInWhiteList != null)
                    return !string.IsNullOrEmpty(incomingCall.OperatorId)
                        ? await GetResponseNumberDiffOperator(from, response)
                        : await GetResponseNumberInWhiteList(from, response);

                var numberInBlackList = await _blackListRepository.GetNumberFromReports(incomingCall.UserId, incomingCall.PhoneNumber);
                if (numberInBlackList != null)
                    return await GetResponseNumberInBlackList(from, response);

                numberInBlackList = await _blackListRepository.GetNumberFromBlackList(incomingCall.PhoneNumber);
                if (numberInBlackList != null)
                    return await GetResponseNumberInBlackList(from, response);

                //string domain = Request.Scheme + "://" + Request.Host.Value + "/api/Call/Check";
                string domain = "http://callblocker.azurewebsites.net/api/Call/Check";
                var gather = new Gather(action: new Uri(domain), timeout: 20, numDigits: 2);

                var files = Directory.GetFiles(_hostingEnvironment.WebRootPath, "*.mp3").Where(x => !x.Contains("FailMessage") && !x.Contains("PassMessage"));
                var randomFile = files.OrderBy(x => Guid.NewGuid()).First();
                var randomNumber = Path.GetFileNameWithoutExtension(randomFile);
                var fileName = Path.GetFileName(randomFile);

                gather.Play(new Uri($"http://callblocker.azurewebsites.net/{fileName}"));

                await _userRepository.AddPhoneNumberCode(from, int.Parse(randomNumber));

                response.Append(gather);
                return new TwiMLResult(response);
            }

            response.Reject();
            return new TwiMLResult(response);
        }

        [HttpPost("[action]")]
        public async Task<TwiMLResult> Check(string from, string digits)
        {
            var response = new VoiceResponse();
            var parsedPhoneNumber = SolutionExtensions.ParsePhoneNumber(from);
            await _devLogRepository.Add(new DevLogModel()
            { Event = "api/Call/Check ", Message = $"[from] = {from}; [parsedPhoneNumber] = {parsedPhoneNumber}; [digits] = " + digits, DateTime = DateTime.UtcNow });

            var userCode = await _userRepository.GetPhoneNumberCodes(parsedPhoneNumber);

            if (userCode != null)
            {
                int.TryParse(digits, out var numbers);
                var incomingCall = await _userRepository.GetIncomingCall(parsedPhoneNumber);

                if (userCode.Code == numbers)
                {
                    if (incomingCall != null)
                    {
                        await _whiteListRepository.AddToWhiteList(incomingCall.UserId, incomingCall.PhoneNumber, null);
                        await _blackListRepository.DeleteFailVerification(incomingCall.PhoneNumber);

                        var correctCodeDailyStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.NewVerified);

                        if (correctCodeDailyStatistic == null)
                            await _statisticRepository.AddDailyStatistic(StatisticType.NewVerified);
                        else
                            await _statisticRepository.UpdateDailyStatistic(StatisticType.NewVerified);

                        response.Play(new Uri($"http://callblocker.azurewebsites.net/PassMessage.mp3"));
                        response.Reject();
                    }
                }
                else
                {
                    CheckFailVerification(incomingCall);

                    var failedCodeDailyStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.FailedByWrondCode);

                    if (failedCodeDailyStatistic == null)
                        await _statisticRepository.AddDailyStatistic(StatisticType.FailedByWrondCode);
                    else
                        await _statisticRepository.UpdateDailyStatistic(StatisticType.FailedByWrondCode);

                    response.Play(new Uri($"http://callblocker.azurewebsites.net/FailMessage.mp3"));
                    response.Reject();
                }
            }

            await _userRepository.DeleteIncomingCall(from);
            await _userRepository.DeletePhoneNumberCode(from);
            return new TwiMLResult(response);
        }

        private async Task CheckFailVerification(NotifyCallModel incomingCall)
        {
            var failVerification = await _blackListRepository.SelectFailVerification(incomingCall.PhoneNumber);

            if (failVerification == null)
                await _blackListRepository.AddFailVerification(incomingCall.PhoneNumber);
            else
            {
                if (failVerification.Count >= SolutionConstants.QueueFailVerification)
                {
                    await _blackListRepository.AddToBlackList(new BlackListPostModel
                    {
                        UserId = incomingCall.UserId,
                        PhoneNumber = incomingCall.PhoneNumber
                    });
                }

                await _blackListRepository.UpdateFailVerification(incomingCall.PhoneNumber,
                    ++failVerification.Count);
            }
        }

        private async Task<TwiMLResult> GetResponseNumberInBlackList(string from, VoiceResponse response)
        {
            await _userRepository.DeleteIncomingCall(from);

            #region Collect statistics

            var blockedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.BlockedCalls);

            if (blockedCallsStatistic == null)
                await _statisticRepository.AddDailyStatistic(StatisticType.BlockedCalls);
            else
                await _statisticRepository.UpdateDailyStatistic(StatisticType.BlockedCalls);
            #endregion

            response.Reject();
            return new TwiMLResult(response);
        }

        private async Task<TwiMLResult> GetResponseNumberInWhiteList(string from, VoiceResponse response)
        {
            await _userRepository.DeleteIncomingCall(from);

            var verifiedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.VerifiedCalls);

            if (verifiedCallsStatistic == null)
                await _statisticRepository.AddDailyStatistic(StatisticType.VerifiedCalls);
            else
                await _statisticRepository.UpdateDailyStatistic(StatisticType.VerifiedCalls);

            response.Say("I am currently busy, please call again later");
            response.Reject();
            return new TwiMLResult(response);
        }

        private async Task<TwiMLResult> GetResponseNumberDiffOperator(string from, VoiceResponse response)
        {
            await _userRepository.DeleteIncomingCall(from);

            #region Collect statistics

            var blockedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.BlockedCalls);

            if (blockedCallsStatistic == null)
                await _statisticRepository.AddDailyStatistic(StatisticType.BlockedCalls);
            else
                await _statisticRepository.UpdateDailyStatistic(StatisticType.BlockedCalls);

            #endregion
            response.Say("Voicemail is disabled, please call again later");
            response.Reject();
            return new TwiMLResult(response);
        }
    }
}