﻿using CallBlocker.Models;
using CallBlocker.Models.BlackList;
using CallBlocker.Repositories.BlackList;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CallBlocker.Models.Enums;
using CallBlocker.Services;

namespace CallBlocker.Controllers.V1
{
    [Route("api/[controller]")]
    public class BlackListController : Controller
    {
        private readonly IBlackListRepository _blackListRepository;
        private readonly IScamService _scamService;

        public BlackListController(IBlackListRepository blackListRepository, IScamService scamService)
        {
            _blackListRepository = blackListRepository;
            _scamService = scamService;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]PhoneModel model)
        {
            if (string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _blackListRepository.DeleteFromBlackList(model.PhoneNumber);
            return Ok();
        }

        [HttpDelete("report/delete")]
        public async Task<IActionResult> DeleteReport([FromBody]ReportModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _blackListRepository.DeleteReportFromBlackList(model.UserId, model.PhoneNumber);
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Report([FromBody]BlackListPostModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _blackListRepository.AddToReport(model);

            if (model.Reason == ReportReason.Scam.ToString())
                _scamService.CheckScamCall(model);
            
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var blackList = await _blackListRepository.GetBlackList();
            return Ok(blackList);
        }

        [HttpGet("[action]/{userId}")]
        public async Task<IActionResult> Reports(string userId)
        {
            var blackList = await _blackListRepository.GetReports(userId);
            return Ok(blackList);
        }
    }
}