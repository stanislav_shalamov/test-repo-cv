﻿using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.BlackList;
using CallBlocker.Models.Enums;
using CallBlocker.Models.Notify;
using CallBlocker.Repositories.BlackList;
using CallBlocker.Repositories.Purchase;
using CallBlocker.Repositories.Statistic;
using CallBlocker.Repositories.User;
using CallBlocker.Repositories.WhiteList;
using Microsoft.AspNetCore.Mvc;

namespace CallBlocker.Controllers.V1
{
    [Route("api/WhiteList")]
    public class WhiteListController : Controller
    {
        private readonly IWhiteListRepository _whiteListRepository;
        private readonly IBlackListRepository _blackListRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly IStatisticRepository _statisticRepository;

        public WhiteListController(IWhiteListRepository whiteListRepository, IUserRepository userRepository,
            IBlackListRepository blackListRepository, IPurchaseRepository purchaseRepository, IStatisticRepository statisticRepository)
        {
            _whiteListRepository = whiteListRepository;
            _userRepository = userRepository;
            _blackListRepository = blackListRepository;
            _purchaseRepository = purchaseRepository;
            _statisticRepository = statisticRepository;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> NotifyCall([FromBody]NotifyCallModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _userRepository.DeleteIncomingCall(model.PhoneNumber);
            await _userRepository.DeleteIncomingCallByUserId(model.UserId);
            await _userRepository.AddIncomingCall(model.UserId, model.PhoneNumber, model.SIM, model.IsContact, model.OperatorId);
            await _userRepository.UpdateActivityDateById(model.UserId);

            #region CommentedSection


            //  if (model.IsContact)
            //  {
            //      var verifiedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.VerifiedCalls);
            //
            //      if (verifiedCallsStatistic == null)
            //          await _statisticRepository.AddDailyStatistic(StatisticType.VerifiedCalls);
            //      else
            //          await _statisticRepository.UpdateDailyStatistic(StatisticType.VerifiedCalls);
            //      return GetPassedResponse(new WhiteListModel());
            //  }
            //
            //  var numberInWhiteList = await _whiteListRepository.GetNumberFromWhiteList(model.UserId, model.PhoneNumber);
            //  if (numberInWhiteList != null)
            //  {
            //      var verifiedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.VerifiedCalls);
            //
            //      if (verifiedCallsStatistic == null)
            //          await _statisticRepository.AddDailyStatistic(StatisticType.VerifiedCalls);
            //      else
            //          await _statisticRepository.UpdateDailyStatistic(StatisticType.VerifiedCalls);
            //
            //      return GetPassedResponse(numberInWhiteList);
            //  }
            //
            //  var numberInBlackList = await _blackListRepository.GetNumberFromReports(model.UserId, model.PhoneNumber);
            //  if (numberInBlackList != null)
            //      return GetFailedresponse(numberInBlackList);
            //
            //  numberInBlackList = await _blackListRepository.GetNumberFromBlackList(model.PhoneNumber);
            //  if (numberInBlackList != null)
            //      return GetFailedresponse(numberInBlackList);
            #endregion

            //save old users
            await _purchaseRepository.OldUserLog(model.UserId);
            var requiredResponse = new OldNotifyResponse<object>();
            requiredResponse.VerificationStatus = Status.Passed;
            return Ok(requiredResponse);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                return BadRequest();

            var whiteList = await _whiteListRepository.GetWhiteList(userId);
            return Ok(whiteList);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]NotifyCallModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _whiteListRepository.DeleteFromWhiteList(model.UserId, model.PhoneNumber);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PostWhiteListModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _whiteListRepository.AddToWhiteList(model.UserId, model.PhoneNumber, model.Name);
            return Ok();
        }

        private IActionResult GetPassedResponse(WhiteListModel model)
        {
            var successResponse = new NotifyResponse<WhiteListModel>();
            successResponse.VerificationStatus = Status.Passed;
            successResponse.AdditionalInfo = model;
            return Ok(successResponse);
        }

        private IActionResult GetFailedresponse(BlackListModel model)
        {
            var failedResponse = new NotifyResponse<BlackListModel>();
            failedResponse.VerificationStatus = Status.Failed;
            failedResponse.AdditionalInfo = model;
            return Ok(failedResponse);
        }
    }
}