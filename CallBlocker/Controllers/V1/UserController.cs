﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.Settings;
using CallBlocker.Repositories.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace CallBlocker.Controllers.V1
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly TwilioSettings _twilioSettings;

        public UserController(IUserRepository userRepository, IOptions<TwilioSettings> twilioSettings)
        {
            _userRepository = userRepository;
            _twilioSettings = twilioSettings.Value;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody]UserLoginModel model)
        {
            var loginModel = new LoginResponseModel();
            loginModel.TwilioNumber = _twilioSettings.TwilioNumber;

            if (string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest(SolutionConstants.PhoneNumberEmpty);

            var regexMatch = Regex.Match(model.PhoneNumber, SolutionConstants.PhoneRegex, RegexOptions.IgnoreCase);

            if(!regexMatch.Success)
                return BadRequest(SolutionConstants.PhoneNumberWrongFormat);

            var userId = await _userRepository.GetUserIdByPhoneNumber(model.PhoneNumber);

            if (!string.IsNullOrEmpty(userId))
                loginModel.UserId = userId;
            else
                loginModel.UserId = await _userRepository.AddNewUser(model.PhoneNumber, model.Password);

            return Ok(loginModel);
        }
    }
}