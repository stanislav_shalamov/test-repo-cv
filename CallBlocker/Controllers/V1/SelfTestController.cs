﻿using System;
using System.Threading.Tasks;
using CallBlocker.Models.SelftTest;
using CallBlocker.Models.Settings;
using CallBlocker.Repositories.SelfTest;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Twilio;
using Twilio.AspNet.Core;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;

namespace CallBlocker.Controllers.V1
{
    [Route("api/SelfTest")]
    public class SelfTestController : Controller
    {
        private readonly ISelfTestRepository _selfTestRepository;
        private readonly TwilioSettings _twilioSettings;
        private readonly IHostingEnvironment _hostingEnvironment;

        public SelfTestController(ISelfTestRepository selfTestRepository, IOptions<TwilioSettings> twilioSettings, IHostingEnvironment hostingEnvironment)
        {
            _selfTestRepository = selfTestRepository;
            _hostingEnvironment = hostingEnvironment;
            _twilioSettings = twilioSettings.Value;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _selfTestRepository.Get());
        }

        [HttpGet("TestNumber")]
        public IActionResult TestNumber()
        {
            return Ok(_twilioSettings.SelfTestNumber);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]SelfTestPostModel model)
        {
            await _selfTestRepository.Add(model);
            return Ok();
        }

        [HttpGet("statistic")]
        public async Task<IActionResult> GetStatistic()
        {
            var statistic = await _selfTestRepository.GetStatistic();
            return Ok(statistic);
        }

        [HttpPost("[action]")]
        public TwiMLResult TwilioSelfTest(string from)
        {
            var response = new VoiceResponse();
            response.Say("");
            response.Reject();

            Task.Factory.StartNew(() => NewCall(from));

            return new TwiMLResult(response);
        }

        [HttpGet("TestDevice/{manufacturer}/{model}/{os}")]
        public async Task<IActionResult> TestDevice(string manufacturer, string model, string os)
        {
            if (string.IsNullOrEmpty(manufacturer) || string.IsNullOrEmpty(model) || string.IsNullOrEmpty(os))
                return BadRequest();

            var isValid = await _selfTestRepository.IsValid(manufacturer, model, os);

            if (isValid)
                return Ok();
            else
                return BadRequest();
        }

        private void NewCall(string from)
        {
            System.Threading.Thread.Sleep(4000);

            TwilioClient.Init(_twilioSettings.Sid, _twilioSettings.Token);
            //string domain = Request.Scheme + "://" + Request.Host.Value + "/selftestmessage.xml";
            string domain = "http://callblocker.azurewebsites.net/selftestmessage.xml";
            var call = CallResource.Create(
                url: new Uri(domain),
                to: new Twilio.Types.PhoneNumber(from),
                from: new Twilio.Types.PhoneNumber(_twilioSettings.SelfTestNumber),
                method: "GET",
                timeout: 25
            );
        }
    }
}
