﻿using System.Linq;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.AccountViewModels;
using CallBlocker.Models.Login;
using CallBlocker.Models.Settings;
using CallBlocker.Repositories.User;
using CallBlocker.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace CallBlocker.Controllers.V2
{
    [Route("api/v2/[controller]")]
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IUserRepository _userRepository;
        private readonly TwilioSettings _twilioSettings;

        public UserController(IUserRepository userRepository, IOptions<TwilioSettings> twilioSettings, 
            UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IEmailSender emailSender)
        {
            _userRepository = userRepository;
            _twilioSettings = twilioSettings.Value;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromBody]UserEmailModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber,
                    VoiceMail = model.VoiceMail, PhoneNumber2 = model.PhoneNumber2, VoiceMail2 = model.VoiceMail2 };

                var existedUser = await _userManager.FindByEmailAsync(model.Email);

                if (existedUser == null)
                {
                    var result = await _userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                        return Ok();

                    return BadRequest(SolutionConstants.WrongPassOrEmail);
                }

                return BadRequest(SolutionConstants.UserAlreadyExists);
            }

            return BadRequest(SolutionConstants.WrondInputData);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody]UserEmailLoginModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, false);

                if (result.Succeeded)
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    var loginResponse = new LoginResponseModel();
                    loginResponse.TwilioNumber = _twilioSettings.TwilioNumber;
                    loginResponse.UserId = user.Id;
                    loginResponse.VoiceMail = user.VoiceMail;
                    loginResponse.SelfTestNumber = _twilioSettings.SelfTestNumber;

                    return Ok(loginResponse);
                }

                return BadRequest(SolutionConstants.WrongPassOrEmail);
            }

            return BadRequest(SolutionConstants.WrondInputData);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ForgotPassword([FromBody]EmailModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                    return BadRequest(SolutionConstants.UserNotExists);

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                if (!string.IsNullOrEmpty(code))
                {
                    var userCode = SolutionExtensions.GetAlpaNumericCharacters(code);
                    await _userRepository.CreateIdentityCodeUserCodeMapping(code, userCode, model.Email);
                  
                    await _emailSender.SendEmailAsync(model.Email, "Reset Password", $"Please enter this code: {userCode}");

                    return Ok();
                }

                return BadRequest();
            }

            return BadRequest(SolutionConstants.WrondInputData);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                    return BadRequest(SolutionConstants.UserNotExists);

                var mapping = (await _userRepository.GeIdentityCodeUserCodeMapping(model.Code, model.Email)).LastOrDefault();
                if(mapping != null)
                {
                    var resetResponse = await _userManager.ResetPasswordAsync(user, mapping.IdentityCode, model.Password);                    
                    if (resetResponse.Succeeded)
                    {
                        await _userRepository.DeleteIdentityCodeUserCodeMapping(model.Email);
                        return Ok();
                    }
                    return BadRequest(SolutionConstants.WrongNewPass);
                }

                return BadRequest(SolutionConstants.DontHaveAccess);
            }

            return BadRequest(SolutionConstants.WrondInputData);
        }
    }
}