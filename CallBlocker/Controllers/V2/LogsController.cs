﻿using System.Threading.Tasks;
using CallBlocker.Models.Logs;
using CallBlocker.Repositories.Logs;
using Microsoft.AspNetCore.Mvc;

namespace CallBlocker.Controllers.V2
{
    [Route("api/v2/[controller]")]
    public class LogsController : Controller
    {
        private readonly ILogRepository _logRepository;

        public LogsController(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(string userId)
        {
            var logs = await _logRepository.Get(userId);
            return Ok(logs);
        }

        [HttpGet("email/{email}")]
        public async Task<IActionResult> GetByEmail(string email)
        {
            var logs = await _logRepository.GetByEmail(email);
            return Ok(logs);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PostLogModel model)
        {
            if (!string.IsNullOrEmpty(model.Message))
            {
                if (model.Message.Contains("isValidPackage") || model.Message.Contains("CallNotification: package name"))
                    return Ok();

                await _logRepository.Create(model);
            }
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]DeleteLogModel model)
        {
            await _logRepository.Delete(model.UserId);
            return Ok();
        }
    }
}
