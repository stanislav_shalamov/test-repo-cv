﻿using System.Threading.Tasks;
using CallBlocker.Repositories.User;
using Microsoft.AspNetCore.Mvc;

namespace CallBlocker.Controllers.V2
{
    [Route("api/v2/NotifyCall")]
    public class NotifyCallController : Controller
    {
        private readonly IUserRepository _userRepository;

        public NotifyCallController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _userRepository.GetNotifyCall());
        }
    }
}
