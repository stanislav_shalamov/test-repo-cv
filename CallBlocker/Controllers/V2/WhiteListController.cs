﻿using System;
using System.Threading.Tasks;
using CallBlocker.Models;
using CallBlocker.Models.BlackList;
using CallBlocker.Models.Enums;
using CallBlocker.Models.Notify;
using CallBlocker.Repositories.BlackList;
using CallBlocker.Repositories.DevLogs;
using CallBlocker.Repositories.Purchase;
using CallBlocker.Repositories.Statistic;
using CallBlocker.Repositories.User;
using CallBlocker.Repositories.WhiteList;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CallBlocker.Controllers.V2
{
    [Route("api/v2/WhiteList")]
    public class WhiteListController : Controller
    {
        private readonly IWhiteListRepository _whiteListRepository;
        private readonly IBlackListRepository _blackListRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly IStatisticRepository _statisticRepository;

        private readonly IDevLogRepository _devLogRepository;

        public WhiteListController(IWhiteListRepository whiteListRepository, IUserRepository userRepository,
            IBlackListRepository blackListRepository, IPurchaseRepository purchaseRepository, IStatisticRepository statisticRepository,
            IDevLogRepository devLogRepository)
        {
            _whiteListRepository = whiteListRepository;
            _userRepository = userRepository;
            _blackListRepository = blackListRepository;
            _purchaseRepository = purchaseRepository;
            _statisticRepository = statisticRepository;
            _devLogRepository = devLogRepository;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> NotifyCall([FromBody]NotifyCallModel model)
        {

            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _userRepository.DeleteIncomingCall(model.PhoneNumber);
            await _userRepository.DeleteIncomingCallByUserId(model.UserId);
            await _userRepository.AddIncomingCall(model.UserId, model.PhoneNumber, model.SIM, model.IsContact, model.OperatorId);
            await _userRepository.UpdateActivityDateById(model.UserId);
                
            await _devLogRepository.Add(new DevLogModel() { Event = "NotifyCall v2", Message = JsonConvert.SerializeObject(model), DateTime = DateTime.UtcNow });

            if (model.NeedCheckSubscriptionStatus)
            {
                //await _purchaseRepository.CreateLogs("NotifyCall v2", JsonConvert.SerializeObject(model));

                if (string.IsNullOrEmpty(model.PurchaseToken))
                    return BadRequest();

                var subscriptionStatus = (SubscriptionStatus)await _purchaseRepository.GetSubscriptionStatus(model.UserId, model.PurchaseToken);

                if (subscriptionStatus == SubscriptionStatus.Subscription_recovered ||
                    subscriptionStatus == SubscriptionStatus.Subscription_renewed ||
                    subscriptionStatus == SubscriptionStatus.Subscription_canceled || 
                    subscriptionStatus == SubscriptionStatus.Subscription_purchased ||
                     subscriptionStatus == SubscriptionStatus.Subscription_in_grace_period || 
                     subscriptionStatus == SubscriptionStatus.Subscription_restarted || 
                     subscriptionStatus == SubscriptionStatus.Subscription_price_change_confirmed)
                    return await ProcessInputCall(model, subscriptionStatus);

                var successResponse = new NotifyResponse<object>();
                successResponse.VerificationStatus = Status.Passed;
                successResponse.AdditionalInfo = new WhiteListModel();
                successResponse.SubscriptionStatus = (int)subscriptionStatus;
                return Ok(successResponse);
            }

            //Only Old users can reach this section
            //await _purchaseRepository.CreateLogs("NotifyCall v2 [old section]", JsonConvert.SerializeObject(model));
            return await ProcessInputCall(model, SubscriptionStatus.Subscription_default, true);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                return BadRequest();

            var whiteList = await _whiteListRepository.GetWhiteList(userId);
            return Ok(whiteList);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]NotifyCallModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _whiteListRepository.DeleteFromWhiteList(model.UserId, model.PhoneNumber);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PostWhiteListModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.PhoneNumber))
                return BadRequest();

            await _whiteListRepository.AddToWhiteList(model.UserId, model.PhoneNumber, model.Name);
            return Ok();
        }

        private async Task<IActionResult> ProcessInputCall(NotifyCallModel model, SubscriptionStatus subscriptionStatus, bool isOldVersion = false)
        {
            if (model.IsContact)
            {
                var verifiedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.VerifiedCalls);
                if (verifiedCallsStatistic == null)
                    await _statisticRepository.AddDailyStatistic(StatisticType.VerifiedCalls);
                else
                    await _statisticRepository.UpdateDailyStatistic(StatisticType.VerifiedCalls);

                return GetPassedResponse(new WhiteListModel(), subscriptionStatus);
            }

            var numberInWhiteList = await _whiteListRepository.GetNumberFromWhiteList(model.UserId, model.PhoneNumber);
            if (numberInWhiteList != null)
            {
                var verifiedCallsStatistic = await _statisticRepository.GetDailyStatisticByType(DateTime.UtcNow, StatisticType.VerifiedCalls);

                if (verifiedCallsStatistic == null)
                    await _statisticRepository.AddDailyStatistic(StatisticType.VerifiedCalls);
                else
                    await _statisticRepository.UpdateDailyStatistic(StatisticType.VerifiedCalls);

                return GetPassedResponse(numberInWhiteList, subscriptionStatus);
            }

            var numberInBlackList = await _blackListRepository.GetNumberFromReports(model.UserId, model.PhoneNumber);
            if (numberInBlackList != null)
                return GetFailedResponse(numberInBlackList, subscriptionStatus);

            numberInBlackList = await _blackListRepository.GetNumberFromBlackList(model.PhoneNumber);
            if (numberInBlackList != null)
                return GetFailedResponse(numberInBlackList, subscriptionStatus);

            if (isOldVersion)
            {
                var oldRequiredResponse = new OldNotifyResponse<object>();
                oldRequiredResponse.VerificationStatus = Status.Required;
                return Ok(oldRequiredResponse);
            }

            var requiredResponse = new NotifyResponse<object>();
            requiredResponse.VerificationStatus = Status.Required;
            requiredResponse.SubscriptionStatus = (int)subscriptionStatus;
            return Ok(requiredResponse);
        }

        private IActionResult GetPassedResponse(WhiteListModel model, SubscriptionStatus subscriptionStatus)
        {
            var successResponse = new NotifyResponse<WhiteListModel>();
            successResponse.VerificationStatus = Status.Passed;
            successResponse.AdditionalInfo = model;
            successResponse.SubscriptionStatus = (int)subscriptionStatus;
            return Ok(successResponse);
        }

        private IActionResult GetFailedResponse(BlackListModel model, SubscriptionStatus subscriptionStatus)
        {
            var failedResponse = new NotifyResponse<BlackListModel>();
            failedResponse.VerificationStatus = Status.Failed;
            failedResponse.AdditionalInfo = model;
            failedResponse.SubscriptionStatus = (int)subscriptionStatus;
            return Ok(failedResponse);
        }
    }
}