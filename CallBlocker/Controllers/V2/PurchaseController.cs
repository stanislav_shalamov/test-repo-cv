﻿using System.Threading.Tasks;
using CallBlocker.Models.Enums;
using CallBlocker.Models.Purchase;
using CallBlocker.Repositories.Purchase;
using CallBlocker.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace CallBlocker.Controllers.V2
{
    [Route("api/v2/user/purchase")]
    public class PurchaseController : Controller
    {

        private readonly IPurchaseRepository _purchaseRepository;
        private readonly PurchaseSettings _purchaseSettings;
        private readonly IPurchaseService _googlePurchaseService;

        public PurchaseController(IPurchaseRepository purchaseRepository, IOptions<PurchaseSettings> purchaseSettings, IPurchaseService googlePurchaseService)
        {
            _purchaseRepository = purchaseRepository;
            _purchaseSettings = purchaseSettings.Value;
            _googlePurchaseService = googlePurchaseService;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Post([FromBody]PurchaseModel model)
        {
            if (string.IsNullOrEmpty(model.ProductId) || string.IsNullOrEmpty(model.Token) || string.IsNullOrEmpty(model.UserId))
                return await SendResponse("PurchaseModel has empty fields", JsonConvert.SerializeObject(model), returnOk: false);

            var purchaseAlreadyExists = await _purchaseRepository.PurchaseAlreadyExist(model.UserId, model.Token);

            if (purchaseAlreadyExists)
                return await SendResponse("  PURCHASE EXISTS", $"UserId = {model.UserId}; PurchaseToken = {model.Token}; ProductId = {model.ProductId}", returnOk: true);

            string refreshToken = await _purchaseRepository.GetRefreshToken();

            //refreshToken is null or empty => Requires authorization https://callblocker.azurewebsites.net/api/v2/user/purchase/Authorization with owner Google account
            if (!string.IsNullOrEmpty(refreshToken))
            {
                GoogleRequestResult googlePurchaseResult = await _googlePurchaseService.ProcessPurchase(_purchaseSettings, refreshToken, model);

                if (!googlePurchaseResult.IsSuccessful)
                    return await SendResponse(googlePurchaseResult.ErrorMessage, googlePurchaseResult.JsonException, returnOk : false);

                var orderIdAlreadyExists = await _purchaseRepository.OrderAlreadyExist(googlePurchaseResult.OrderId);

                if (orderIdAlreadyExists)
                    return await SendResponse($"PURCHASE IS NOT VALID. [OrderId] Already exists. UserId = {model.UserId}; OrderId = {googlePurchaseResult.OrderId}; " +
                        $"PurchaseToken = {model.Token}; ProductId = {model.ProductId}", googlePurchaseResult.JsonResponse, returnOk : false);

                await _purchaseRepository.Add(model.UserId, model.Token, (int)SubscriptionStatus.Subscription_purchased, googlePurchaseResult.OrderId);
                await _purchaseRepository.CreateLogs($"NEW PURCHASE: UserId = {model.UserId}; PurchaseToken = {model.Token}; SubscriptionStatus = 4; OrderId = {googlePurchaseResult.OrderId}; " +
                    $"ProductId = {model.ProductId}", "step 3: PurchaseDetails was saved");
                return Ok();
            }
            return await SendResponse(string.Empty, "RefreshToken is null or empty", returnOk: false);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Authorization(string code)
        {
            ViewBag.IsSuccessful = false;
            if (!string.IsNullOrEmpty(code))
            {
                string requestData = $"grant_type=authorization_code&code={code}&client_id={_purchaseSettings.ClientId}&client_secret={_purchaseSettings.ClientSecret}&redirect_uri={_purchaseSettings.RedirectURI}";
                WebRequestBuilder myRequest = new WebRequestBuilder("https://accounts.google.com/o/oauth2/token", requestData);
                var responseModel = await myRequest.GetResponse();

                if (responseModel.IsSuccessful)
                {
                    await _purchaseRepository.CreateLogs(responseModel.JsonResponse, "AuthorizationResponse");
                    GoogleAccessModel accessTokenModel = JsonConvert.DeserializeObject<GoogleAccessModel>(responseModel.JsonResponse);

                    if (!string.IsNullOrEmpty(accessTokenModel.Refresh_token))
                    {
                        await _purchaseRepository.CreateAuthCode(accessTokenModel);
                        ViewBag.IsSuccessful = true;
                    }
                    else
                        ViewBag.ErrorMessage = "An authorization code was received, but a response from Google Play Console does not contain a refresh token";
                }
                else
                {
                    if (!string.IsNullOrEmpty(responseModel.JsonError))
                        await _purchaseRepository.CreateLogs(responseModel.JsonError, "AuthorizationResponse");
                    else
                        await _purchaseRepository.CreateLogs(responseModel.ErrorMessage, "AuthorizationResponse");

                    ViewBag.ErrorMessage = "Something was wrong with your URL";
                }
            }
            else
                ViewBag.ErrorMessage = "An authorization code was empty";

            return View();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> PostPubSubMessage([FromBody]PubSubEncodedModel pubSubEncodedModel)
        {
            string refreshToken = await _purchaseRepository.GetRefreshToken();

            //refreshToken is null or empty => Requires authorization https://callblocker.azurewebsites.net/api/v2/user/purchase/Authorization with owner Google account
            if (!string.IsNullOrEmpty(refreshToken))
            {
                PubSubDecodedModel pubSubDecodedModel = JsonConvert.DeserializeObject<PubSubDecodedModel>(SolutionExtensions.Base64Decode(pubSubEncodedModel.Message.Data));
                GoogleRequestResult googleRequestResult = await _googlePurchaseService.ProcessPubSubMessage(pubSubDecodedModel, _purchaseSettings, refreshToken);

                if (!googleRequestResult.IsSuccessful)
                    return await SendResponse(googleRequestResult.ErrorMessage, googleRequestResult.JsonException, true);

                await _purchaseRepository.Update(pubSubDecodedModel.SubscriptionNotification.NotificationType, googleRequestResult.OrderId);
                await _purchaseRepository.CreateLogs($"UPDATED [SubscriptionStatus] = {pubSubDecodedModel.SubscriptionNotification.NotificationType} " +
                    $"WHERE [OrderId] = {googleRequestResult.OrderId} [PurchaseToken] = {pubSubDecodedModel.SubscriptionNotification.PurchaseToken}", 
                    "PostPubSubMessage step 3: Updated");
            }
            else
                await _purchaseRepository.CreateLogs("RefreshToken is null or empty", "PubSubMessage");
            return Ok();
        }

        private async Task<IActionResult> SendResponse(string head, string body, bool returnOk)
        {
            await _purchaseRepository.CreateLogs(head, body);
            if (returnOk)
                return Ok();
            return BadRequest();
        }
    }
}