﻿using CallBlocker.Quartz;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CallBlocker
{
    public static class QuartzServicesUtilities
    {
        public static void StartJob<TJob>(IScheduler scheduler) where TJob : IJob
        {
            var jobName = typeof(TJob).FullName;

            var job = JobBuilder.Create<TJob>()
                .WithIdentity(jobName)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity($"{jobName}.trigger")
                .StartNow()
                .WithSimpleSchedule(scheduleBuilder =>
                    scheduleBuilder
                        .WithIntervalInHours(240)
                        .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger).Wait();
        }

        //Delete Logs
        public static void StartMidnightJob<TJob>(IScheduler scheduler) where TJob : IJob
        {
            var jobName = typeof(TJob).FullName;

            var job = JobBuilder.Create<TJob>()
                .WithIdentity(jobName)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity($"{jobName}.trigger")
                .StartNow()
                .WithCronSchedule("00 59 06 ? * *", cron => { cron.InTimeZone(TimeZoneInfo.Utc); })
                .Build();

            scheduler.ScheduleJob(job, trigger).Wait();
        }

        //DISABLED
        public static void StartMiddayJob<TJob>(IScheduler scheduler) where TJob : IJob
        {
            var jobName = typeof(TJob).FullName;

            var job = JobBuilder.Create<TJob>()
                .WithIdentity(string.Concat(jobName, "midday"))
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity($"{jobName}midday.trigger")
                .StartNow()
                .WithCronSchedule("59 59 11 ? * *", cron => { cron.InTimeZone(TimeZoneInfo.Utc); })
                .Build();

            scheduler.ScheduleJob(job, trigger).Wait();
        }

        //Developer Notification
        internal static void StartEverydayDeveloperStatisticJob<TJob>(IScheduler scheduler) where TJob : IJob
        {
            var jobName = typeof(TJob).FullName;

            var job = JobBuilder.Create<TJob>()
                .WithIdentity(string.Concat(jobName))
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity($"{jobName}.trigger")
                .StartNow()
                .WithCronSchedule("00 45 06 ? * *", cron => { cron.InTimeZone(TimeZoneInfo.Utc); })
                .Build();

            scheduler.ScheduleJob(job, trigger).Wait();
        }

        public static void StartEverydayStatisticJob<TJob>(IScheduler scheduler) where TJob : IJob
        {
            var jobName = typeof(TJob).FullName;

            var job = JobBuilder.Create<TJob>()
                .WithIdentity(string.Concat(jobName))
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity($"{jobName}.trigger")
                .StartNow()
                .WithCronSchedule("00 55 06 ? * *", cron => { cron.InTimeZone(TimeZoneInfo.Utc); })
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static async Task RescheduleJob(string time)
        {
            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            var jobGroups = await scheduler.GetJobGroupNames();

            foreach (var group in jobGroups)
            {
                var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                var jobKeys = await scheduler.GetJobKeys(groupMatcher);

                foreach (var jobKey in jobKeys)
                {
                    if (jobKey.Name == typeof(SendTodaysStatisticJob).FullName)
                    {
                        ChangeJobInterval(jobKey, time);
                    }
                }
            }
        }

        public static void ChangeJobInterval(JobKey key, string newTime)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;

            var trigger = (scheduler.GetTriggersOfJob(key).Result).ElementAt(0);
            scheduler.RescheduleJob(trigger.Key, CreateTrigger(trigger, newTime));
        }

        private static ITrigger CreateTrigger(ITrigger oldTrigger, string newTime)
        {
            var builder = oldTrigger.GetTriggerBuilder();

            builder = builder.StartNow().WithCronSchedule(newTime, cron => { cron.InTimeZone(TimeZoneInfo.Utc); });

            var newTrigger = builder.Build();

            return newTrigger;
        }

        public static async Task<string> GetJobInterval()
        {
            string cronExpression = string.Empty;
            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            var jobGroups = await scheduler.GetJobGroupNames();

            foreach (var group in jobGroups)
            {
                var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                var jobKeys = await scheduler.GetJobKeys(groupMatcher);

                foreach (var job in jobKeys)
                {
                    if (job.Name == typeof(SendTodaysStatisticJob).FullName)
                    {
                        var triggers = await scheduler.GetTriggersOfJob(job);
                        foreach (ITrigger triger in triggers)
                        {
                            cronExpression = ((ICronTrigger)triger).CronExpressionString;
                        }
                    }
                }
            }
            return cronExpression;
        }
    }
}
