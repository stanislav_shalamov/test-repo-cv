﻿using CallBlocker.Models;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CallBlocker
{
    public static class SolutionExtensions
    {
        public static string CreateMD5(string input)
        {
            using (var md5 = MD5.Create())
            {
                var inputBytes = Encoding.ASCII.GetBytes(input);
                var hashBytes = md5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                foreach (var t in hashBytes)
                {
                    sb.Append(t.ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static string TruncateLongString(this string str, int maxLength)
        {
            return string.IsNullOrEmpty(str) ? str : str.Substring(25, Math.Min(str.Length, maxLength));
        }

        public static string GetAlpaNumericCharacters(string code)
        {
            if (!string.IsNullOrEmpty(code) && code.Length > 40)
                return new string(code.Where(c => char.IsLetterOrDigit(c) || char.IsWhiteSpace(c) || c == '-').ToArray()).TruncateLongString(8);

            return code;
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string ParsePhoneNumber(string phoneNumber)
        {
            if (!phoneNumber.Contains("+1"))
                return phoneNumber;

            return phoneNumber.Remove(0, 2);
        }
    }
}
